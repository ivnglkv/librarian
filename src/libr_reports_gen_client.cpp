#include "libr_reports_gen_client.h"
#include "QTime"

LibrReportsGenClient::LibrReportsGenClient(const QString &strHost, int nPort, QObject *parent) :
    QObject(parent),
    nextBlockSize(0)
{
    tcpSocket = new QTcpSocket(this);
    tcpSocket->connectToHost(strHost, nPort);
    connect(tcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(slotError(QAbstractSocket::SocketError)));
}

void LibrReportsGenClient::slotReadyRead()
{
    QString resHtml;
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_8);
    for (;;) {
        QString tmpHtml;
        if (!nextBlockSize) {
            if (tcpSocket->bytesAvailable() < sizeof(quint16))
                break;
            in >> nextBlockSize;
        }

        if (tcpSocket->bytesAvailable() < nextBlockSize)
            break;

        in >> tmpHtml;
        resHtml += tmpHtml;
        nextBlockSize = 0;
    }
    emit gotAnswer(resHtml);
    return;
}

void LibrReportsGenClient::slotSendToServer(ReportTypes type, uint id)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_8);
    out << quint16(0) << (type + id * 10);

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    tcpSocket->write(arrBlock);
}

void LibrReportsGenClient::slotError(QAbstractSocket::SocketError)
{
    qDebug() << "Fucking error";
}
