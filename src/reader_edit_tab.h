#ifndef READER_EDIT_TAB_H
#define READER_EDIT_TAB_H

#include <QtGui>
#include <QtUiTools>

#include "ui_reader_edit_tab.h"
#include "reader.h"
#include "messages.h"
#include "convert_date.h"
#include "validators.h"

namespace Ui {
class ReaderEditTabForm;
}

class ReaderEditTabForm : public QWidget
{
    Q_OBJECT
public:
    explicit ReaderEditTabForm(const int id, QWidget *parent = 0);
    ~ReaderEditTabForm();

private slots:
    void on_okButton_clicked();
    void on_otherBox_currentIndexChanged(int index);

private:
    Ui::ReaderEditTabForm *ui;
    Reader *reader;

signals:
    void readerUpdated(QString);
};

#endif // READER_EDIT_TAB_H
