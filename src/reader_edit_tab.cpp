#include "reader_edit_tab.h"

ReaderEditTabForm::ReaderEditTabForm(const int id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReaderEditTabForm),
    reader(new Reader)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

//    FIO group
    QLineEdit *fstNameEdit = this->findChild<QLineEdit*>("fstNameEdit");
    NameValidator *fstNameValidator = new NameValidator(fstNameEdit);
    fstNameEdit->setValidator(fstNameValidator);

    QLineEdit *secNameEdit = this->findChild<QLineEdit*>("secNameEdit");
    NameValidator *secNameValidator = new NameValidator(secNameEdit);
    secNameEdit->setValidator(secNameValidator);

    QLineEdit *patNameEdit = this->findChild<QLineEdit*>("patNameEdit");
    NameValidator *patNameValidator = new NameValidator(patNameEdit);
    patNameEdit->setValidator(patNameValidator);

//    passport group
    QLineEdit *passportSeriesEdit = this->findChild<QLineEdit*>("passportSeriesEdit");
    NumberValidator *passportSeriesValidator = new NumberValidator(passportSeriesEdit);
    passportSeriesEdit->setValidator(passportSeriesValidator);

    QLineEdit *passportNumberEdit = this->findChild<QLineEdit*>("passportNumberEdit");
    NumberValidator *passportNumberValidator = new NumberValidator(passportNumberEdit);
    passportNumberEdit->setValidator(passportNumberValidator);

    QLineEdit *passportWhoEdit = this->findChild<QLineEdit*>("passportWhoEdit");

    QDateEdit *passportDateEdit = this->findChild<QDateEdit*>("passportDateEdit");
    passportDateEdit->setMaximumDate(QDate::currentDate());

//    other document group
    QComboBox *otherBox = this->findChild<QComboBox*>("otherBox");

    QLineEdit *otherSeriesEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
    NumberValidator *otherSeriesValidator = new NumberValidator(otherSeriesEdit);
    otherSeriesEdit->setValidator(otherSeriesValidator);

    QLineEdit *otherNumberEdit = this->findChild<QLineEdit*>("otherNumberEdit");
    NumberValidator *otherNumberValidator = new NumberValidator(otherNumberEdit);
    otherNumberEdit->setValidator(otherNumberValidator);

    QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");

    QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
    otherDateEdit->setMaximumDate(QDate::currentDate());

//    area group
    QLineEdit *areaEdit = this->findChild<QLineEdit*>("areaEdit");
    QLineEdit *cityEdit = this->findChild<QLineEdit*>("cityEdit");
    QLineEdit *streetEdit = this->findChild<QLineEdit*>("streetEdit");
    QLineEdit *houseEdit = this->findChild<QLineEdit*>("houseEdit");
    NumberValidator *houseValidator = new NumberValidator(houseEdit);
    houseEdit->setValidator(houseValidator);
    QLineEdit *pavEdit = this->findChild<QLineEdit*>("pavEdit");
    QLineEdit *roomEdit = this->findChild<QLineEdit*>("roomEdit");
    NumberValidator *roomValidator = new NumberValidator(roomEdit);
    roomEdit->setValidator(roomValidator);

//    phones group
    QLineEdit *homePhoneEdit = this->findChild<QLineEdit*>("homePhoneEdit");
    PhoneValidator *homePhoneValidator = new PhoneValidator(homePhoneEdit);
    homePhoneEdit->setValidator(homePhoneValidator);
    QLineEdit *mobPhoneEdit = this->findChild<QLineEdit*>("mobPhoneEdit");
    PhoneValidator *mobPhoneValidator = new PhoneValidator(mobPhoneEdit);
    mobPhoneEdit->setValidator(mobPhoneValidator);

    QSqlQuery getReader;
    getReader.prepare("SELECT * FROM readers WHERE idReader = :id");
    getReader.bindValue(":id", id);

    if (!getReader.exec()) {
        qDebug() << "Unable to make select reader operation";
        errorMessage(QObject::tr("Не удалось найти запись в базу данных.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0120"));
        return;
    }

    QSqlRecord rec = getReader.record();
    int fstNameCol = rec.indexOf("fstName");
    int secNameCol = rec.indexOf("secName");
    int patNameCol = rec.indexOf("patName");
    int passSerCol = rec.indexOf("passSer");
    int passNumCol = rec.indexOf("passNum");
    int passWhoCol = rec.indexOf("passWho");
    int passWhenCol = rec.indexOf("passWhen");
    int otherCol = rec.indexOf("other");
    int otherSerCol = rec.indexOf("otherSer");
    int otherNumCol = rec.indexOf("otherNum");
    int otherWhoCol = rec.indexOf("otherWho");
    int otherWhenCol = rec.indexOf("otherWhen");
    int areaCol = rec.indexOf("area");
    int cityCol = rec.indexOf("city");
    int streetCol = rec.indexOf("street");
    int houseCol = rec.indexOf("house");
    int pavCol = rec.indexOf("pav");
    int roomCol = rec.indexOf("room");
    int homePhoneCol = rec.indexOf("homePhone");
    int mobilePhoneCol = rec.indexOf("mobilePhone");

    getReader.next();

    reader->setIdReader(id);
    reader->setFstName(getReader.value(fstNameCol).toString());
    reader->setSecName(getReader.value(secNameCol).toString());
    if (getReader.value(patNameCol).toString() == "0")
        reader->setPatName("");
    else
        reader->setPatName(getReader.value(patNameCol).toString());
    reader->setPassSer(getReader.value(passSerCol).toInt());
    reader->setPassNum(getReader.value(passNumCol).toInt());
    reader->setPassWho(getReader.value(passWhoCol).toString());
    reader->setPassWhen(getReader.value(passWhenCol).toDate());
    reader->setOther(getReader.value(otherCol).toString());
    reader->setOtherSer(getReader.value(otherSerCol).toInt());
    reader->setOtherNum(getReader.value(otherNumCol).toInt());
    reader->setOtherWho(getReader.value(otherWhoCol).toString());
    reader->setOtherWhen(getReader.value(otherWhenCol).toDate());
    reader->setArea(getReader.value(areaCol).toString());
    reader->setCity(getReader.value(cityCol).toString());
    reader->setStreet(getReader.value(streetCol).toString());
    reader->setHouse(getReader.value(houseCol).toInt());
    reader->setPav(getReader.value(pavCol).toString());
    reader->setRoom(getReader.value(roomCol).toInt());
    reader->setHomePhone(getReader.value(homePhoneCol).toString());
    reader->setMobilePhone(getReader.value(mobilePhoneCol).toString());

    fstNameEdit->setText(reader->getFstName());
    secNameEdit->setText(reader->getSecName());
    if (reader->getPatName() == "")
        patNameEdit->setText(reader->getPatName());
    else
        patNameEdit->setText(reader->getPatName());

    if (reader->getPassSer() == -1) {
        passportSeriesEdit->setText("");
        passportNumberEdit->setText("");
        passportWhoEdit->setText("");
    } else {
        passportSeriesEdit->setText(QString::number(reader->getPassSer()));
        passportNumberEdit->setText(QString::number(reader->getPassNum()));
        passportWhoEdit->setText(reader->getPassWho());
        passportDateEdit->setDate(reader->getPassWhen());
    }

    if (reader->getOther() == "") {
        otherBox->setCurrentIndex(0);
    } else {
        qDebug() << reader->getOther();
        int otherIndex = otherBox->findText(reader->getOther());
        otherBox->setCurrentIndex(otherIndex);
        if (otherIndex > 0) {
            otherSeriesEdit->setText(QString::number(reader->getOtherSer()));
            otherNumberEdit->setText(QString::number(reader->getOtherNum()));
            otherWhoEdit->setText(reader->getOtherWho());
            otherDateEdit->setDate(reader->getOtherWhen());
        }
    }

    areaEdit->setText(reader->getArea());
    cityEdit->setText(reader->getCity());
    streetEdit->setText(reader->getStreet());
    houseEdit->setText(QString::number(reader->getHouse()));

    if (reader->getPav() == "0") {
        pavEdit->setText("");
    } else {
        pavEdit->setText(reader->getPav());
    }

    if (reader->getRoom() == 0) {
        roomEdit->setText("");
    } else {
        roomEdit->setText(QString::number(reader->getRoom()));
    }

    if (reader->getHomePhone() == "0") {
        homePhoneEdit->setText("");
    } else {
        homePhoneEdit->setText(reader->getHomePhone());
    }

    if (reader->getMobilePhone() == "0") {
        mobPhoneEdit->setText("");
    } else {
        mobPhoneEdit->setText(reader->getMobilePhone());
    }
}

ReaderEditTabForm::~ReaderEditTabForm()
{
    delete ui;
    delete reader;
}

void ReaderEditTabForm::on_okButton_clicked()
{
    QLineEdit *fstNameEdit = this->findChild<QLineEdit*>("fstNameEdit");
    if (fstNameEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите фамилию читателя.\n"
                                 "Код ошибки: 1005"));
        return;
    }
    reader->setFstName(fstNameEdit->text());

    QLineEdit *secNameEdit = this->findChild<QLineEdit*>("secNameEdit");
    if (secNameEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите имя читателя.\n"
                                 "Код ошибки: 1006"));
        return;
    }
    reader->setSecName(secNameEdit->text());

    QLineEdit *patNameEdit = this->findChild<QLineEdit*>("patNameEdit");
    reader->setPatName(patNameEdit->text());

    QLineEdit *passSerEdit = this->findChild<QLineEdit*>("passportSeriesEdit");
    QLineEdit *passNumEdit = this->findChild<QLineEdit*>("passportNumberEdit");
    QLineEdit *passWhoEdit = this->findChild<QLineEdit*>("passportWhoEdit");
    QDateEdit *passDateEdit = this->findChild<QDateEdit*>("passportDateEdit");
    QComboBox *otherBox = this->findChild<QComboBox*>("otherBox");

    if (passSerEdit->text().isEmpty() && passNumEdit->text().isEmpty() &&
            passWhoEdit->text().isEmpty() && (otherBox->currentIndex() != 6)) {
        errorMessage(QObject::tr("Необходимо ввести паспортные данные.\n"
                                 "Код ошибки: 1007"));
        return;
    } else if (passSerEdit->text().isEmpty() && passNumEdit->text().isEmpty() &&
               passWhoEdit->text().isEmpty() && (otherBox->currentIndex() == 6)) {
        reader->setPassSer(-1);

        reader->setOther(otherBox->currentText());

        QLineEdit *otherSerEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        if (otherSerEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите серию документа.\n"
                                     "Код ошибки: 1008"));
            return;
        } else {
            reader->setOtherSer(otherSerEdit->text().toInt());
        }

        QLineEdit *otherNumEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        if (otherNumEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите номер документа.\n"
                                     "Код ошибки: 1009"));
            return;
        } else {
            reader->setOtherNum(otherNumEdit->text().toInt());
        }

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        reader->setOtherWhen(otherDateEdit->date());

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        if (otherWhoEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите название организации выдавшей документ.\n"
                                     "Код ошибки: 1010"));
            return;
        } else {
            reader->setOtherWho(otherWhoEdit->text());
        }
    }

    if (reader->getPassSer() != -1) {
        if (passSerEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите серию паспорта.\n"
                                     "Код ошибки: 1011"));
            return;
        }
        reader->setPassSer(passSerEdit->text().toInt());

        if (passNumEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите номер паспорта.\n"
                                     "Код ошибки: 1012"));
            return;
        }
        reader->setPassNum(passNumEdit->text().toInt());

        if (passWhoEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите название организации, выдавшей паспорт.\n"
                                     "Код ошибки: 1013"));
            return;
        }
        reader->setPassWho(passWhoEdit->text());

        reader->setPassWhen(passDateEdit->date());
    }

    if (otherBox->currentIndex() != 6 &&
            otherBox->currentIndex() != 0) {
        reader->setOther(otherBox->currentText());

        QLineEdit *otherSerEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        if (otherSerEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите серию документа.\n"
                                     "Код ошибки: 1014"));
            return;
        }
        reader->setOtherSer(otherSerEdit->text().toInt());

        QLineEdit *otherNumEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        if (otherNumEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите номер документа.\n"
                                     "Код ошибки: 1015"));
            return;
        }
        reader->setOtherNum(otherNumEdit->text().toInt());

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        reader->setOtherWhen(otherDateEdit->date());

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        if (otherWhoEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите название организации выдавшей документ.\n"
                                     "Код ошибки: 1016"));
            return;
        }
        reader->setOtherWho(otherWhoEdit->text());
    } else if (otherBox->currentIndex() == 0)
        reader->setOther("");

    QLineEdit *areaEdit = this->findChild<QLineEdit*>("areaEdit");
    if (areaEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите область проживания.\n"
                                 "Код ошибки: 1017"));
        return;
    }
    reader->setArea(areaEdit->text());

    QLineEdit *cityEdit = this->findChild<QLineEdit*>("cityEdit");
    if (cityEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите название населенного пункта, в котором проживает читатель.\n"
                                 "Код ошибки: 1018"));
        return;
    }
    reader->setCity(cityEdit->text());

    QLineEdit *streetEdit = this->findChild<QLineEdit*>("streetEdit");
    if (streetEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите название улицы, на которой проживает читатель.\n"
                                 "Код ошибки: 1019"));
        return;
    }
    reader->setStreet(streetEdit->text());

    QLineEdit *houseEdit = this->findChild<QLineEdit*>("houseEdit");
    if (houseEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите номер дома, в котором проживает читатель.\n"
                                 "Код ошибки: 1020"));
        return;
    }
    reader->setHouse(houseEdit->text().toInt());

    QLineEdit *pavEdit = this->findChild<QLineEdit*>("pavEdit");
    reader->setPav(pavEdit->text());

    QLineEdit *roomEdit = this->findChild<QLineEdit*>("roomEdit");
    reader->setRoom(roomEdit->text().toInt());

    QLineEdit *homePhoneEdit = this->findChild<QLineEdit*>("homePhoneEdit");
    reader->setHomePhone(homePhoneEdit->text());

    QLineEdit *mobPhoneEdit = this->findChild<QLineEdit*>("mobPhoneEdit");
    reader->setMobilePhone(mobPhoneEdit->text());

    if (homePhoneEdit->text().isEmpty() && mobPhoneEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Необходимо ввести хотя бы один телефон.\n"
                                 "Код ошибки: 1021"));
        return;
    }

    QString confirmString = QObject::tr("Обновить информацию о читателе?");
    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    if (reader->updateReader()) {
        QString message = QObject::tr("Обновлена информация о читателе №");
        message += QString::number(reader->getIdReader())
                + " ("
                + reader->getFstName() + " "
                + reader->getSecName() + " "
                + reader->getPatName()
                + ")";
        emit readerUpdated(message);
    }
}

void ReaderEditTabForm::on_otherBox_currentIndexChanged(int index)
{
    if (index <= 0) {
        QLineEdit *otherSeriesEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        otherSeriesEdit->setEnabled(false);

        QLineEdit *otherNumberEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        otherNumberEdit->setEnabled(false);

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        otherDateEdit->setEnabled(false);

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        otherWhoEdit->setEnabled(false);
    }
    else {
        QLineEdit *otherSeriesEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        otherSeriesEdit->setEnabled(true);

        QLineEdit *otherNumberEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        otherNumberEdit->setEnabled(true);

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        otherDateEdit->setEnabled(true);

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        otherWhoEdit->setEnabled(true);
    }
}
