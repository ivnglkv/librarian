#ifndef REPORT_TYPES_H
#define REPORT_TYPES_H

enum ReportTypes {
    UNICBOOKS = 0x1,
    GIVBOOKS,
    READERS,
    HISTORY,
    DEBTS
};

#endif // REPORT_TYPES_H
