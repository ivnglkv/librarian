#ifndef ADD_BOOK_FORM_H
#define ADD_BOOK_FORM_H

#include <QWidget>
#include "book.h"
#include "ui_add_book_form.h"

namespace Ui {
class AddBookForm;
}

class AddBookForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit AddBookForm(QWidget *parent = 0);
    ~AddBookForm();
    
private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::AddBookForm *ui;
    Book *book;
    void keyPressEvent(QKeyEvent *);

signals:
    void bookAdded(QString);
};

#endif // ADD_BOOK_FORM_H
