#ifndef REMOVE_READER_FORM_H
#define REMOVE_READER_FORM_H

#include "reader.h"
#include "ui_remove_reader_form.h"

namespace Ui {
class RemoveReaderForm;
}

class RemoveReaderForm : public QWidget
{
    Q_OBJECT
public:
    explicit RemoveReaderForm(QWidget *parent = 0);
    ~RemoveReaderForm();
    
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::RemoveReaderForm *ui;
    Reader *reader;

    void keyPressEvent(QKeyEvent *);

signals:
    void readerRemoved(QString);
};

#endif // REMOVE_READER_FORM_H
