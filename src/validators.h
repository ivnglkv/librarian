#ifndef VALIDATORS_H
#define VALIDATORS_H

#include <QValidator>
#include <QRegExp>

class NameValidator : public QValidator
{
public:
    NameValidator(QObject *parent) : QValidator(parent)
    {
    }

    virtual State validate(QString &str, int &pos) const
    {
        if (pos == 0){};
        QRegExp rxp = QRegExp("[0-9!;:\\.\\$\\[\\]\\^\\*\\+\\?\\,\\@\\/\\{\\}"
                              "\\#\\%\\&\\(\\)\\_\\\\\"\\<\\>\\~\\`\\|\\+\\=]");

        if (str.contains(rxp))
            return Invalid;
        return Acceptable;
    }
};

class NumberValidator : public QValidator
{
public:
    NumberValidator(QObject *parent) : QValidator(parent)
    {
    }

    virtual State validate(QString &str, int &pos) const
    {
        if (pos == 0){};
        QRegExp rxp = QRegExp("\\D");

        if (str.contains(rxp))
            return Invalid;
        return Acceptable;
    }
};

class PhoneValidator : public QValidator
{
public:
    PhoneValidator(QObject *parent) : QValidator(parent)
    {
    }

    virtual State validate(QString &str, int &pos) const
    {
        if (pos == 0){};
        QRegExp rxp = QRegExp("[^-\\d\\+\\s]");

        if (str.contains(rxp))
            return Invalid;
        return Acceptable;
    }
};

class AuthorNameValidator : public QValidator
{
public:
    AuthorNameValidator(QObject *parent) : QValidator(parent)
    {
    }

    virtual State validate(QString &str, int &pos) const
    {
        if (pos == 0){};
        QRegExp rxp = QRegExp("[0-9!;:\\$\\[\\]\\^\\*\\+\\?\\@\\/\\{\\}"
                              "\\#\\%\\&\\(\\)\\_\\\\\"\\<\\>\\~\\`\\|\\+\\=]");

        if (str.contains(rxp))
            return Invalid;
        return Acceptable;
    }
};

#endif // VALIDATORS_H
