#include "give_book_form.h"

GiveBookForm::GiveBookForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GiveBookForm),
    reader(new Reader),
    book(new Book)
{
    ui->setupUi(this);
}

GiveBookForm::~GiveBookForm()
{
    delete ui;
}

void GiveBookForm::on_buttonBox_accepted()
{
    QSpinBox *idReaderBox = this->findChild<QSpinBox*>("idReaderBox");
    if (!reader->setIdReader(idReaderBox->value()))
        return;

    QSpinBox *idBookBox = this->findChild<QSpinBox*>("idBookBox");
    if (!book->setId(idBookBox->value()))
        return;

    QSpinBox *countBox = this->findChild<QSpinBox*>("countBox");
    uint count = countBox->value();

    QString confirmString = QObject::tr("Выдать читателю №");
    confirmString += QString::number(reader->getIdReader())
            + " ("
            + reader->getFstName() + " "
            + reader->getSecName() + " "
            + reader->getPatName() + ") "
            + QString::number(count) + " ";
    switch (count % 10) {
    case 1:
        if (count != 11) {
            confirmString += QObject::tr("книгу №");
            break;
        }
    case 2:
    case 3:
    case 4:
        if ((count != 11)
                && (count != 12)
                && (count != 13)
                && (count != 14)) {
            confirmString += QObject::tr(" книги №");
            break;
        }
    default:
        confirmString += QObject::tr(" книг №");
        break;
    }
    confirmString += QString::number(book->getId())
            + " ("
            + book->getName()
            + ") "
            + QObject::tr("?");
    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    if (reader->giveBook(book->getId(), count)) {
        QString message = QObject::tr("Читателю №");
        message += QString::number(reader->getIdReader());
        if (((count % 10) != 1) || (count == 11)) {
            message += QObject::tr(" выдано ");
        } else {
            message += QObject::tr(" выдана ");
        }
        message += QString::number(count);

        switch (count % 10) {
        case 1:
            if (count != 11) {
                message += QObject::tr(" книга №");
                break;
            }
        case 2:
        case 3:
        case 4:
            if ((count != 12)
                    && (count != 13)
                    && (count != 14)) {
                message += QObject::tr(" книги №");
                break;
            }
        default:
            message += QObject::tr(" книг №");
            break;
        }
        message += QString::number(book->getId());
        emit bookGived(message);
    }
}

void GiveBookForm::on_buttonBox_rejected()
{
    this->close();
}

void GiveBookForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Enter:
    case Qt::Key_Return:
        if (pe->modifiers() & Qt::ControlModifier)
            on_buttonBox_accepted();
        break;
    case Qt::Key_Escape:
        on_buttonBox_rejected();
        break;
    default:
        break;
    }
}
