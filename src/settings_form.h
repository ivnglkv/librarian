#ifndef SETTINGS_FORM_H
#define SETTINGS_FORM_H

#include "interface_settings_form.h"
#include "users_settings_form.h"
#include "network_settings_form.h"
#include "organization_settings_form.h"
#include "ui_settings_form.h"

namespace Ui {
class SettingsForm;
}

class SettingsForm : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsForm(QWidget *parent = 0);
    ~SettingsForm();
private slots:
    void on_groupsList_itemClicked(QListWidgetItem *item);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::SettingsForm *ui;

signals:
    void saveSettings();
    void rejectSettings();
};

#endif // SETTINGS_FORM_H
