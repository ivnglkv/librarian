#include "remove_reader_form.h"

RemoveReaderForm::RemoveReaderForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RemoveReaderForm),
    reader(new Reader)
{
    ui->setupUi(this);
}

RemoveReaderForm::~RemoveReaderForm()
{
    delete reader;
    delete ui;
}

void RemoveReaderForm::on_buttonBox_accepted()
{
    QSpinBox *idEdit = this->findChild<QSpinBox*>("idEdit");
    if (!reader->setIdReader(idEdit->value()))
        return;

    if (confirmationMessage(QObject::tr("Вы действительно хотите удалить профиль читателя №")
                            + QString::number(reader->getIdReader())
                            + " ("
                            + reader->getFstName() + " "
                            + reader->getSecName() + " "
                            + reader->getPatName()
                            + ")?") == QMessageBox::No)
        return;

    if (reader->removeReader()) {
        QString message = QObject::tr("Удален профиль читателя №")
                + QString::number(reader->getIdReader());
        emit readerRemoved(message);
        this->close();
    }
}

void RemoveReaderForm::on_buttonBox_rejected()
{
    this->close();
}

void RemoveReaderForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Enter:
    case Qt::Key_Return:
        if (pe->modifiers() & Qt::ControlModifier)
            on_buttonBox_accepted();
        break;
    case Qt::Key_Escape:
        on_buttonBox_rejected();
        break;
    default:
        break;
    }
}
