#include "reader_profile.h"

ReaderProfileForm::ReaderProfileForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReaderProfileForm)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);
}

ReaderProfileForm::~ReaderProfileForm()
{
    delete ui;
}

void ReaderProfileForm::showProfile(int id)
{
    QLabel *idLbl = this->findChild<QLabel*>("id");
    idLbl->setText(QString::number(id));

    QSqlQuery selectReader;
    selectReader.prepare("SELECT * FROM readers WHERE idReader = :id");
    selectReader.bindValue(":id", id);

    if (!selectReader.exec()) {
        qDebug() << "Error occured while trying to execute select operation";
        errorMessage(QObject::tr("Не удалось произвести поиск.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0121"));
        return;
    }
    selectReader.next();

    QSqlRecord reader = selectReader.record();
    int fstNameCol = reader.indexOf("fstName");
    int secNameCol = reader.indexOf("secName");
    int patNameCol = reader.indexOf("patName");
    int passSerCol = reader.indexOf("passSer");
    int passNumCol = reader.indexOf("passNum");
    int passWhoCol = reader.indexOf("passWho");
    int passWhenCol = reader.indexOf("passWhen");
    int otherCol = reader.indexOf("other");
    int otherSerCol = reader.indexOf("otherSer");
    int otherNumCol = reader.indexOf("otherNum");
    int otherWhoCol = reader.indexOf("otherWho");
    int otherWhenCol = reader.indexOf("otherWhen");
    int areaCol = reader.indexOf("area");
    int cityCol = reader.indexOf("city");
    int streetCol = reader.indexOf("street");
    int houseCol = reader.indexOf("house");
    int pavCol = reader.indexOf("pav");
    int roomCol = reader.indexOf("room");
    int homePhoneCol = reader.indexOf("homePhone");
    int mobilePhoneCol = reader.indexOf("mobilePhone");

    QLabel *fstName = this->findChild<QLabel*>("fstName");
    fstName->setText(selectReader.value(fstNameCol).toString());
    QLabel *secName = this->findChild<QLabel*>("secName");
    secName->setText(selectReader.value(secNameCol).toString());
    QLabel *patName = this->findChild<QLabel*>("patName");
    patName->setText(selectReader.value(patNameCol).toString());
    QLabel *passSer = this->findChild<QLabel*>("passSer");
    QLabel *passNum = this->findChild<QLabel*>("passNum");
    QLabel *passWho = this->findChild<QLabel*>("passWho");
    QLabel *passWhen = this->findChild<QLabel*>("passWhen");
    if (selectReader.value(passSerCol).toInt() == 0) {
        passSer->setText("");
        passNum->setText("");
        passWho->setText("");
        passWhen->setText("");
    } else {
        passSer->setText(QString::number(selectReader.value(passSerCol).toInt()));
        passNum->setText(QString::number(selectReader.value(passNumCol).toInt()));
        passWho->setText(selectReader.value(passWhoCol).toString());
        passWhen->setText(selectReader.value(passWhenCol).toDate().toString("d.M.yyyy"));
    }
    QLabel *other = this->findChild<QLabel*>("other");
    QLabel *otherSer = this->findChild<QLabel*>("otherSer");
    QLabel *otherNum = this->findChild<QLabel*>("otherNum");
    QLabel *otherWho = this->findChild<QLabel*>("otherWho");
    QLabel *otherWhen = this->findChild<QLabel*>("otherWhen");
    if (selectReader.value(otherCol).toString() == "") {
        other->setText("");
        otherSer->setText("");
        otherNum->setText("");
        otherWho->setText("");
        otherWhen->setText("");
    } else {
        other->setText(selectReader.value(otherCol).toString());
        otherSer->setText(QString::number(selectReader.value(otherSerCol).toInt()));
        otherNum->setText(QString::number(selectReader.value(otherNumCol).toInt()));
        otherWho->setText(selectReader.value(otherWhoCol).toString());
        otherWhen->setText(selectReader.value(otherWhenCol).toDate().toString("d.M.yyyy"));
    }
    QLabel *area = this->findChild<QLabel*>("area");
    area->setText(selectReader.value(areaCol).toString());
    QLabel *city = this->findChild<QLabel*>("city");
    city->setText(selectReader.value(cityCol).toString());
    QLabel *street = this->findChild<QLabel*>("street");
    street->setText(selectReader.value(streetCol).toString());
    QLabel *house = this->findChild<QLabel*>("home");
    house->setText(QString::number(selectReader.value(houseCol).toInt()));
    QLabel *pav = this->findChild<QLabel*>("pav");
    pav->setText(selectReader.value(pavCol).toString());
    QLabel *room = this->findChild<QLabel*>("room");
    if (selectReader.value(roomCol).toInt() == 0) {
        room->setText("");
    } else {
        room->setText(QString::number(selectReader.value(roomCol).toInt()));
    }
    QLabel *homePhone = this->findChild<QLabel*>("homePhone");
    homePhone->setText(selectReader.value(homePhoneCol).toString());
    QLabel *mobilePhone = this->findChild<QLabel*>("mobilePhone");
    mobilePhone->setText(selectReader.value(mobilePhoneCol).toString());
}

void ReaderProfileForm::on_editReaderButton_clicked()
{
    QLabel *idLbl = this->findChild<QLabel*>("id");
    int id = idLbl->text().toInt();
    Reader reader;
    if (!reader.setIdReader(id))
        return;
    emit editReader(id);
}

void ReaderProfileForm::on_showHistoryButton_clicked()
{
    QLabel *idLbl = this->findChild<QLabel*>("id");
    int id = idLbl->text().toInt();
    Reader reader;
    if (!reader.setIdReader(id))
        return;

    QString rowsCountQuery = "SELECT COUNT(id) AS total FROM history "
            "WHERE idReader = "
            + QString::number(id);
    QString getDataQuery = "SELECT books.name, DATE_FORMAT(dateTime, '%d.%m.%Y, %T'), "
            "operation, history.count FROM history, books "
            "WHERE idReader = "
            + QString::number(id) +
            " AND idBook = books.id ORDER BY dateTime DESC "
            "LIMIT :lines";
    QString btnsQuery = "SELECT books.name, DATE_FORMAT(dateTime, '%d.%m.%Y, %T'), "
            "operation, history.count FROM history, books "
            "WHERE idReader = "
            + QString::number(id) +
            " AND idBook = books.id ORDER BY dateTime DESC "
            "LIMIT :pos, :lines";
    QString columnsName[] = {QObject::tr("Название книги"),
                             QObject::tr("Дата"),
                             QObject::tr("Операция"),
                             QObject::tr("Количество")
    };
    int columnsWidth[] = {400,
                          100,
                          100,
                          100
    };
    int countCol = 4;
    QString title = QObject::tr("История действий читателя");
    QString errorStr = QObject::tr("Не удалось выполнить запрос на получение "
                                   "истории действий читателя");
    int maxRows = 10;

    TableForm *bf = new TableForm (rowsCountQuery, getDataQuery, btnsQuery, columnsName,
                                   columnsWidth, countCol, title, errorStr, maxRows, HISTORY, id);
    bf->showMaximized();
}

void ReaderProfileForm::on_debtorBooksBtn_clicked()
{
    QLabel *idLbl = this->findChild<QLabel*>("id");
    int id = idLbl->text().toInt();
    Reader reader;
    if (!reader.setIdReader(id))
        return;

    QString rowsCountQuery = "SELECT COUNT(idReader) AS total FROM taken_books WHERE idReader = "
            + QString::number(id);
    QString getDataQuery = "SELECT books.name, taken_books.count FROM books, taken_books "
            "WHERE idReader = " + QString::number(id)
            + " AND taken_books.idBook = books.id LIMIT :lines";
    QString btnsQuery = "SELECT books.name, taken_books.count FROM books, taken_books "
            "WHERE idReader = " + QString::number(id)
            + " AND taken_books.idBook = books.id LIMIT :pos, :lines";
    QString columnsName[] = {QObject::tr("Название книги"),
                             QObject::tr("Количество")
    };
    int columnsWidth[] = {400,
                          100
    };
    int countCol = 2;
    QString title = QObject::tr("Несданные книги читателя №") + QString::number(id);
    QString errorStr = QObject::tr("Не удалось выполнить запрос на получение "
                                   "истории действий читателя");
    int maxRows = 10;

    TableForm *bf = new TableForm (rowsCountQuery, getDataQuery, btnsQuery, columnsName,
                                   columnsWidth, countCol, title, errorStr, maxRows, DEBTS, id);
    bf->showMaximized();
}

void ReaderProfileForm::on_printDebtsBtn_clicked()
{
    QString sHtml;
    QLabel *idLbl = this->findChild<QLabel*>("id");
    QLabel *fstName = this->findChild<QLabel*>("fstName");
    QLabel *secName = this->findChild<QLabel*>("secName");
    QLabel *patName = this->findChild<QLabel*>("patName");

    int id = idLbl->text().toInt();
    QString fstNameStr = fstName->text();
    QString secNameStr = secName->text();
    QString patNameStr = patName->text();
    Reader reader;
    if (!reader.setIdReader(id))
        return;

    sHtml  = "<!DOCTYPE HTML>";
    sHtml += "<html>";
    sHtml += "<head>";
    sHtml += "<meta charset=\"UTF-8\">";
    sHtml += "</head>";
    sHtml += "<body>";

    QSettings *settings = Librarian::librarianApp()->settings();
    sHtml += "<div id=\"header\">";
    sHtml += QObject::tr(settings->value("/Organization/Name",
                                         "Не задано название библиотеки").toString().toUtf8().constData());
    sHtml += "</div>";
    sHtml += "<h3>";
    sHtml += QObject::tr("Задолженности читателя №") + QString::number(id);
    sHtml += " (";
    sHtml += QObject::tr(fstNameStr.toUtf8().constData()) + " ";
    sHtml += QObject::tr(secNameStr.toUtf8().constData()) + " ";
    sHtml += QObject::tr(patNameStr.toUtf8().constData()) + ")";
    sHtml += "</h3>";
    sHtml += "<table border=\"2\" cellpadding=\"5\" style=\"border-collapse: collapse\">";
    sHtml += "<tr>";
    sHtml += "<th>";
    sHtml += QObject::tr("№ п/п");
    sHtml += "</th>";
    sHtml += "<th>";
    sHtml += QObject::tr("Название книги");
    sHtml += "</th>";
    sHtml += "<th>";
    sHtml += QObject::tr("Количество");
    sHtml += "</th>";
    sHtml += "</tr>";

    QSqlQuery query;
    query.prepare("SELECT books.name, taken_books.count FROM books, taken_books "
                  "WHERE idReader = :idReader AND taken_books.idBook = books.id");
    query.bindValue(":idReader", id);
    if (!query.exec()) {
        errorMessage(QObject::tr("Не удалось выполнить запрос на получение истории "
                                 "действий читателя"));
        return;
    }

    QSqlRecord rec = query.record();
    QString bookName;
    int takenBooksCount;
    int i = 1;
    int totalCount = 0;

    while (query.next()) {
        bookName = query.value(rec.indexOf("name")).toString();
        takenBooksCount = query.value(rec.indexOf("count")).toInt();
        sHtml += "<tr>";
        sHtml += "<td align=\"right\" width=\"10%\">";
        sHtml += QString::number(i++);
        sHtml += "</td>";
        sHtml += "<td width=\"80%\">";
        sHtml += bookName;
        sHtml += "</td>";
        sHtml += "<td width=\"10%\">";
        sHtml += QString::number(takenBooksCount);
        sHtml += "</td>";
        sHtml += "</tr>";
        totalCount += takenBooksCount;
    }

    sHtml += "</table>";
    sHtml += "<p>";
    sHtml += QObject::tr("Итого: ");
    sHtml += QString::number(totalCount);
    switch (totalCount % 10) {
    case 1:
        if (totalCount != 11) {
            sHtml += QObject::tr(" книга");
            break;
        }
    case 2:
    case 3:
    case 4:
        if ((totalCount != 11)
                && (totalCount != 12)
                && (totalCount != 13)
                && (totalCount != 14)) {
            sHtml += QObject::tr(" книги");
            break;
        }
    default:
        sHtml += QObject::tr(" книг");
        break;
    }
    sHtml += "</p>";

    QDateTime dt;
    sHtml += "<div id=\"footer\">";
    sHtml += dt.currentDateTime().toString("d MMMM yyyy, hh:mm");
    sHtml += "</div>";

    sHtml += "</body>";
    sHtml += "</html>";

    LibrPrint lp;
    lp.printPreview(sHtml);
    return;
}
