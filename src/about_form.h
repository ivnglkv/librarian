#ifndef ABOUT_FORM_H
#define ABOUT_FORM_H

#include <QWidget>

namespace Ui {
class AboutForm;
}

class AboutForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit AboutForm(QWidget *parent = 0);
    ~AboutForm();
    
private:
    Ui::AboutForm *ui;
};

#endif // ABOUT_FORM_H
