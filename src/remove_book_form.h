#ifndef REMOVE_BOOK_FORM_H
#define REMOVE_BOOK_FORM_H

#include <QWidget>

#include "book.h"
#include "ui_remove_book_form.h"

namespace Ui {
class RemoveBookForm;
}

class RemoveBookForm : public QWidget
{
    Q_OBJECT
public:
    explicit RemoveBookForm(QWidget *parent = 0);
    ~RemoveBookForm();
    
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::RemoveBookForm *ui;
    Book *book;

    void keyPressEvent(QKeyEvent *);

signals:
    void bookRemoved(QString);
    
};

#endif // REMOVE_BOOK_FORM_H
