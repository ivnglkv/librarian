#ifndef CONVERT_DATE_TO_MYSQL_FORMAT_H
#define CONVERT_DATE_TO_MYSQL_FORMAT_H

#include <QString>
#include <QStringList>

// converting date
// from dd.mm.yyyy format to yyyy-mm-dd format
void toMySql(QString &);

#endif // CONVERT_DATE_TO_MYSQL_FORMAT_H
