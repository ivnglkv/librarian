#include "messages.h"

// Show error message with text given in parameter
void errorMessage(const QString &text)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QMessageBox::critical(0,
                          QObject::tr("Ошибка"),
                          text,
                          QMessageBox::Ok,
                          QMessageBox::Ok);
    return;
}

void warningMessage(const QString &text)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QMessageBox::warning(0,
                         QObject::tr("Ошибка"),
                         text,
                         QMessageBox::Ok,
                         QMessageBox::Ok);
    return;
}

int confirmationMessage(const QString &text)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    int answer = QMessageBox::warning(0,
                                      QObject::tr("Подтвердите выполнение"),
                                      text,
                                      QMessageBox::Yes | QMessageBox::No,
                                      QMessageBox::No);
    return answer;
}
