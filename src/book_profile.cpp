#include "book_profile.h"

BookProfileForm::BookProfileForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookProfileForm)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);
}

BookProfileForm::~BookProfileForm()
{
    delete ui;
}

void BookProfileForm::showProfile(int idBook)
{
    QLabel *id = this->findChild<QLabel*>("id");
    id->setText(QString::number(idBook));

    QSqlQuery selectBook;
    selectBook.prepare("SELECT * FROM books WHERE id = :id");
    selectBook.bindValue(":id", idBook);

    if (!selectBook.exec()) {
        qDebug() << "Error occured while trying to execute select operation";
        errorMessage(QObject::tr("Не удалось произвести поиск.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0108"));
        return;
    }
    selectBook.next();

    QSqlQuery selectAuthors;
    selectAuthors.prepare("SELECT name FROM authors WHERE id IN "
                          "(SELECT idAuthor FROM books_authors WHERE idBook = :id)");
    selectAuthors.bindValue(":id", idBook);

    if (!selectAuthors.exec()) {
        qDebug() << "Error: can't select authors for book " << idBook;
        qDebug() << selectAuthors.lastError();
        errorMessage(QObject::tr("Не удалось найти авторов\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0109"));
        return;
    }

    QSqlRecord authors = selectAuthors.record();
    int authorNameCol = authors.indexOf("name");
    QLabel *authorsLbl = this->findChild<QLabel*>("authors");
    authorsLbl->setText("");

    while (selectAuthors.next()) {
        QString authorsStr = authorsLbl->text()
                + selectAuthors.value(authorNameCol).toString();
        selectAuthors.next();
        if (selectAuthors.isValid())
            authorsStr += ", ";
        selectAuthors.previous();
        authorsLbl->setText(authorsStr);
    }

    QSqlRecord book = selectBook.record();
    int nameCol = book.indexOf("name");
    int countCol = book.indexOf("count");
    int isbnCol = book.indexOf("isbn");
    int udkCol = book.indexOf("udk");
    int bbkCol = book.indexOf("bbk");
    int publisherNameCol = book.indexOf("publisherName");
    int editionNumCol = book.indexOf("editionNum");
    int editionYearCol = book.indexOf("editionYear");

    QLabel *name = this->findChild<QLabel*>("name");
    name->setText(selectBook.value(nameCol).toString());
    QLabel *count = this->findChild<QLabel*>("count");
    count->setText(selectBook.value(countCol).toString());
    QLabel *isbn = this->findChild<QLabel*>("isbn");
    isbn->setText(selectBook.value(isbnCol).toString());
    QLabel *udk = this->findChild<QLabel*>("udk");
    udk->setText(selectBook.value(udkCol).toString());
    QLabel *bbk = this->findChild<QLabel*>("bbk");
    bbk->setText(selectBook.value(bbkCol).toString());
    QLabel *publisherName = this->findChild<QLabel*>("publisherName");
    publisherName->setText(selectBook.value(publisherNameCol).toString());
    QLabel *editionNum = this->findChild<QLabel*>("editionNum");
    editionNum->setText(selectBook.value(editionNumCol).toString());
    QLabel *editionYear = this->findChild<QLabel*>("editionYear");
    editionYear->setText(QString::number(selectBook.value(editionYearCol).toDate().year()));

    QSqlQuery selectGiven;
    selectGiven.prepare("SELECT count FROM taken_books WHERE idBook = :idBook");
    selectGiven.bindValue(":idBook", idBook);
    if (!selectGiven.exec()) {
        qDebug() << "Error: can't select number of taken books " << idBook;
        qDebug() << selectGiven.lastError();
        errorMessage(QObject::tr("Не удалось вычислить количество выданных книг.\n"
                                 "Код ошибки: 0110"));
        return;
    }

    selectGiven.next();
    int givenNum= 0;
    if (selectGiven.isValid()) {
        QSqlRecord givenRecord = selectGiven.record();
        int givenCountCol = givenRecord.indexOf("count");
        do {
            givenNum += selectGiven.value(givenCountCol).toInt();
        } while (selectGiven.next());
    }

    QLabel *givenCount = this->findChild<QLabel*>("givenCount");
    givenCount->setText(QString::number(givenNum));

    QLabel *totalCount = this->findChild<QLabel*>("totalCount");
    int totalNum = selectBook.value(countCol).toInt() + givenNum;
    totalCount->setText(QString::number(totalNum));
}

void BookProfileForm::on_editBookButton_clicked()
{
    QLabel *idLbl = this->findChild<QLabel*>("id");
    if (idLbl->text().isEmpty())
        return;
    QLabel *authors = this->findChild<QLabel*>("authors");
    QString authorsStr = authors->text();
    int id = idLbl->text().toInt();
    emit editBook(id, authorsStr);
}
