#ifndef SEARCH_READER_TAB_H
#define SEARCH_READER_TAB_H

#include <QtGui>
#include <QtUiTools>

#include "ui_search_reader_tab.h"
#include "reader_profile.h"

namespace Ui {
class SearchReaderTabForm;
}

class SearchReaderTabForm : public QWidget
{
    Q_OBJECT
public:
    explicit SearchReaderTabForm(QWidget *parent = 0);
    ~SearchReaderTabForm();

private slots:
    void on_searchButton_clicked();
    void on_shortResultsView_clicked(const QModelIndex &index);
    void editBookRedirect(int id);

signals:
    void showReaderProfile(int id);
    void editBook(int id);
    void readersFound(QString);

private:
    Ui::SearchReaderTabForm *ui;
};

#endif // SEARCH_READER_TAB_H
