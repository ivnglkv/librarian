#include "add_book_form.h"
#include "ui_add_book_form.h"

AddBookForm::AddBookForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddBookForm),
    book (new Book)
{
    ui->setupUi(this);
}

AddBookForm::~AddBookForm()
{
    delete ui;
}

void AddBookForm::on_buttonBox_accepted()
{
    QSpinBox *bookIdBox = this->findChild<QSpinBox*>("bookIdBox");
    if (!book->setId(bookIdBox->value()))
        return;

    QSpinBox *bookCountBox = this->findChild<QSpinBox*>("bookCountBox");
    uint count = bookCountBox->value();

    QString confirmString = QObject::tr("Добавить ");
    confirmString += QString::number(count);
    switch (count % 10) {
    case 1:
        if (count != 11) {
            confirmString += QObject::tr(" книгу №");
            break;
        }
    case 2:
    case 3:
    case 4:
        if ((count != 11)
                && (count != 12)
                && (count != 13)
                && (count != 14)) {
            confirmString += QObject::tr(" книги №");
            break;
        }
    default:
        confirmString += QObject::tr(" книг №");
        break;
    }
    confirmString += QString::number(book->getId())
            + " ("
            + book->getName()
            + ")";

    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    if (book->addBook(count)) {
        QString message;
        if (count == 1)
            message = QObject::tr("Добавлена ");
        else
            message = QObject::tr("Добавлено ");
        message += QString::number(count);
        switch (count % 10) {
        case 1:
            if (count != 11) {
                message += QObject::tr(" книга №");
                break;
            }
        default:
            if (((count % 10) != 0)
                    && (count != 11)
                    && (count != 12)
                    && (count != 13)
                    && (count != 14)) {
                message += QObject::tr(" книги №");
            } else {
                message += QObject::tr(" книг №");
            }
            break;
        }
        message += QString::number(book->getId());
        emit bookAdded(message);
    }
}

void AddBookForm::on_buttonBox_rejected()
{
    this->close();
}

void AddBookForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        if (pe->modifiers() & Qt::ControlModifier) {
            on_buttonBox_accepted();
            break;
        }
        break;
    case Qt::Key_Escape:
        this->close();
        break;
    default:
        break;
    }
}
