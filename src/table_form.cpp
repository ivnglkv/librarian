#include "table_form.h"

// Функция создает форму, выводящую данные из таблицы
// rowsCountQuery - строка запроса для получения количества записей в таблице
// getDataQuery - строка запроса для получения нужных записей из таблицы
// btnsQuery - запрос, который будет выполняться при нажатии на кнопки "Предыдущие"/"Следующие"
// columnsMap - словарь, содержащий названия столбцов и их ширину (для отображения в таблице)
// title - заголовок формы
// errorStr - строка ошибки
// maxRows - количество строк таблицы, которое может быть выведено за один раз
TableForm::TableForm(QString rowsCountQuery, QString getDataQuery,
                     QString btnsQuery, QString columnsName[], int columnsWidth[], int countCol,
                     QString title, QString errorStr, int maxRows, ReportTypes type, uint id) :
    ui(new Ui::TableForm)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

    QPushButton *prevBtn = this->findChild<QPushButton*>("prevBtn");
    QPushButton *nextBtn = this->findChild<QPushButton*>("nextBtn");
    QPushButton *printBtn = this->findChild<QPushButton*>("printBtn");
    if (!query.exec(rowsCountQuery)) {
        prevBtn->setEnabled(false);
        nextBtn->setEnabled(false);
        printBtn->setEnabled(false);
        errorMessage(QObject::tr(qPrintable(errorStr)));
        qDebug() << query.lastError();
        return;
    }

    query.next();
    totalCount = query.value(query.record().indexOf("total")).toInt();
    query.prepare(getDataQuery);
    query.bindValue(":lines", maxRows);
    if (!query.exec()) {
        prevBtn->setEnabled(false);
        nextBtn->setEnabled(false);
        printBtn->setEnabled(false);
        errorMessage(errorStr);
        qDebug() << query.lastError();
        return;
    }
    curNum = 0;

    model = new QSqlQueryModel;
    model->setQuery(query);

    for (int i = 0; i < countCol; ++i)
        model->setHeaderData(i, Qt::Horizontal, QObject::tr(qPrintable(columnsName[i])));

    QTableView *tableView = this->findChild<QTableView*>("tableView");
    tableView->setModel(model);

    for (int i = 0; i < countCol; ++i)
        tableView->setColumnWidth(i, columnsWidth[i]);

    tableView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    tableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    // Сразу после открытия формы нельзя посмотреть предыдущие записи
    prevBtn->setEnabled(false);

    // Если количество выведенных записей меньше или равно количеству записей,
    // получаемых за запрос, то кнопка "Следующие" должна быть неактивна
    if (totalCount <= maxRows) {
        nextBtn->setEnabled(false);
    }

    if (totalCount == 0) {
        printBtn->setEnabled(false);
    }

    QLabel *resLbl = this->findChild<QLabel*>("resLbl");
    resLbl->setText(resLbl->text()
                    + " 1-" + QString::number(maxRows > totalCount ? totalCount : maxRows)
                    + QObject::tr(" из ") + QString::number(totalCount));

    this->setWindowTitle(title);
    bQuery = btnsQuery;
    errStr = errorStr;
    mRows = maxRows;
    this->type = type;
    this->id = id;
}

TableForm::~TableForm()
{
    delete ui;
}

void TableForm::on_nextBtn_clicked()
{
    query.prepare(bQuery);
    query.bindValue(":pos", curNum + mRows);
    query.bindValue(":lines", mRows);
    if (!query.exec()) {
        errorMessage(errStr);
        return;
    }
    curNum += mRows;

    QPushButton *prevBtn = this->findChild<QPushButton*>("prevBtn");
    QPushButton *nextBtn = this->findChild<QPushButton*>("nextBtn");
    if (curNum >= totalCount - mRows)
        nextBtn->setEnabled(false);

    if (!prevBtn->isEnabled())
        prevBtn->setEnabled(true);

    model->setQuery(query);

    QLabel *resLbl = this->findChild<QLabel*>("resLbl");
    resLbl->setText(QObject::tr("Записи: ")
                    + QString::number(curNum + 1)
                    + "-"
                    + QString::number(totalCount - curNum < mRows ? totalCount : curNum + mRows)
                    + QObject::tr(" из ")
                    + QString::number(totalCount));
}

void TableForm::on_prevBtn_clicked()
{
    query.prepare(bQuery);
    query.bindValue(":pos", curNum - mRows);
    query.bindValue(":lines", mRows);
    if (!query.exec()) {
        errorMessage(errStr);
        return;
    }
    curNum -= mRows;

    QPushButton *prevBtn = this->findChild<QPushButton*>("prevBtn");
    QPushButton *nextBtn = this->findChild<QPushButton*>("nextBtn");
    if (curNum == 0)
        prevBtn->setEnabled(false);

    if (!nextBtn->isEnabled())
        nextBtn->setEnabled(true);

    model->setQuery(query);

    QLabel *resLbl = this->findChild<QLabel*>("resLbl");
    resLbl->setText(QObject::tr("Записи: ")
                    + QString::number(curNum + 1)
                    + "-"
                    + QString::number(totalCount - curNum < mRows ? totalCount : curNum + mRows)
                    + QObject::tr(" из ")
                    + QString::number(totalCount));
}

void TableForm::on_printBtn_clicked()
{
    LibrReportsGenClient* repClient = new LibrReportsGenClient("localhost", 2323);
    connect(repClient, SIGNAL(gotAnswer(QString)),
            this, SLOT(onPrintRequested(QString)));
    repClient->slotSendToServer(type, id);
}

void TableForm::onPrintRequested(QString sHtml)
{
    LibrPrint lp;
    lp.printPreview(sHtml);
    return;
}
