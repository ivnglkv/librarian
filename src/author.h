#ifndef AUTHOR_H
#define AUTHOR_H

#include <QList>
#include <QString>
#include <QtSql>

#include "messages.h"

class Author
{
public:
    Author();
    uint add();

    void setId(const uint);
    uint getId();
    void setName(const QString &);
    QString getName();

    uint find();
    int searchBook(const uint);
    int addBook(const uint);

private:
    uint id;
    QString name;
};

#endif // AUTHOR_H
