#include "libr_print.h"

LibrPrint::LibrPrint(QObject *parent) :
    QObject(parent)
{
    webView = new QWebView;
    webView->setVisible(false);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
}

LibrPrint::~LibrPrint()
{
    delete webView;
}

void LibrPrint::printPreview(QString sHtml)
{
#ifndef QT_NO_PRINTDIALOG
    webView->setHtml(sHtml);  //передаем буфер в браузер
    webView->settings()->setUserStyleSheetUrl
            (QUrl::fromLocalFile(Librarian::librarianApp()->applicationDirPath() + "/print.css"));
    QPrinter printer;
    prevDlg = new QPrintPreviewDialog(&printer, 0, Qt::Window); //создаем диалог предварительного просмотра
    //соединяем сигнал, активизирующий рисование, с выводом на печать
    connect(prevDlg, SIGNAL(paintRequested(QPrinter*)), webView, SLOT(print(QPrinter*)));
    //запускаем предварительный просмотр
    prevDlg->setWindowState(Qt::WindowMaximized);
    prevDlg->setWindowTitle(QObject::tr("Печать"));
    prevDlg->setWindowIcon(QPixmap(":icons/librarian.png"));
    //prevDlg->setWindowIcon(QIcon::addPixmap(QPixmap(":/icons/librarian.png")));
    prevDlg->exec();
    delete prevDlg;
 #endif
}
