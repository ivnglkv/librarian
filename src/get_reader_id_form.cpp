#include "get_reader_id_form.h"

GetReaderIdForm::GetReaderIdForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GetReaderIdForm)
{
    ui->setupUi(this);
}

GetReaderIdForm::~GetReaderIdForm()
{
    delete ui;
}


void GetReaderIdForm::on_buttonBox_accepted()
{
    QSpinBox *idEdit = this->findChild<QSpinBox*>("idEdit");
    int id = idEdit->value();
    QSqlQuery query;
    query.prepare("SELECT * FROM readers WHERE idReader = :id");
    query.bindValue(":id", id);
    if (!query.exec()) {
        errorMessage(QObject::tr("Не удалось выполнить запрос на существование читетеля "
                                 "c таким id.\n"
                                 "Код ошибки: 0113"));
        return;
    }
    query.next();
    if (!query.isValid()) {
        warningMessage(QObject::tr("В базе данных нет читателя с таким id"));
        return;
    }
    emit editReaderRequested(id);
    this->close();
}

void GetReaderIdForm::on_buttonBox_rejected()
{
    this->close();
}

void GetReaderIdForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Enter:
    case Qt::Key_Return:
        if (pe->modifiers() & Qt::ControlModifier)
            on_buttonBox_accepted();
        break;
    case Qt::Key_Escape:
        on_buttonBox_rejected();
        break;
    default:
        break;
    }
}
