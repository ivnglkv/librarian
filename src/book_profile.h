#ifndef BOOK_PROFILE_H
#define BOOK_PROFILE_H

#include <QtGui>
#include <QtUiTools>
#include <QtSql>

#include "ui_book_profile.h"
#include "messages.h"

namespace Ui {
class BookProfileForm;
}

class BookProfileForm : public QWidget
{
    Q_OBJECT
public:
    explicit BookProfileForm(QWidget *parent = 0);
    ~BookProfileForm();
public slots:
    void showProfile(int);
signals:
    void editBook(int id, QString &);
private slots:
    void on_editBookButton_clicked();

private:
    Ui::BookProfileForm *ui;
};

#endif // BOOK_PROFILE_H
