#ifndef READER_PROFILE_H
#define READER_PROFILE_H

#include <QtGui>
#include <QtUiTools>
#include <QtSql>
#include <QDateTime>

#include "reader.h"
#include "ui_reader_profile.h"
#include "messages.h"
#include "table_form.h"
#include "libr_print.h"
#include "librarian.h"
#include "report_types.h"

namespace Ui {
class ReaderProfileForm;
}

class ReaderProfileForm : public QWidget
{
    Q_OBJECT
public:
    explicit ReaderProfileForm(QWidget *parent = 0);
    ~ReaderProfileForm();
public slots:
    void showProfile(int);
signals:
    void editReader(int id);
private slots:
    void on_editReaderButton_clicked();
    void on_showHistoryButton_clicked();
    void on_debtorBooksBtn_clicked();
    void on_printDebtsBtn_clicked();

private:
    Ui::ReaderProfileForm *ui;
};

#endif // READER_PROFILE_H
