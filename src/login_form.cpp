#include "login_form.h"

LoginForm::LoginForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginForm)
{
    ui->setupUi(this);

    QLineEdit *passEdit = this->findChild<QLineEdit*>("passEdit");
    passEdit->setEchoMode(QLineEdit::Password);

    QSettings *settings = Librarian::librarianApp()->settings();
    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    nameEdit->setText(settings->value("/Database/Username").toString());
}

LoginForm::~LoginForm()
{
    delete ui;
}

void LoginForm::on_okButton_clicked()
{
    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    if (nameEdit->text().isEmpty()) {
        warningMessage(QObject::tr("Введите имя пользователя"));
        return;
    }

    QLineEdit *passEdit = this->findChild<QLineEdit*>("passEdit");
    if (passEdit->text().isEmpty()) {
        warningMessage(QObject::tr("Введите пароль пользователя"));
        return;
    }

    QSettings *settings = Librarian::librarianApp()->settings();
    settings->setValue("/Database/Username", nameEdit->text());

    QString name = nameEdit->text();
    QString pass = passEdit->text();
    if (createConnection(name, pass)) {
        qDebug() << "Info: login successful";
        this->close();
    } else {
        errorMessage(QObject::tr("Не удалось подключиться к базе данных.\n"
                                 "Код ошибки: 1501"));
        return;
    }
}

void LoginForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        on_okButton_clicked();
        break;
    default:
        break;
    }
}
