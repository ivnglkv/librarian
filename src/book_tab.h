#ifndef BOOKTAB_H
#define BOOKTAB_H

#include <QtGui>
#include <QtUiTools>

#include "ui_book_tab.h"
#include "book.h"
#include "author.h"
#include "validators.h"
#include "messages.h"

namespace Ui {
class BookTabForm;
}

class BookTabForm : public QWidget
{
    Q_OBJECT
public:
    explicit BookTabForm(QWidget *parent = 0);
    ~BookTabForm();
    
private slots:
    void on_okButton_clicked();

private:
    Ui::BookTabForm *ui;
    Book *book;
    Author *author;

signals:
    void newBookAdded(QString);
};

#endif // BOOKTAB_H
