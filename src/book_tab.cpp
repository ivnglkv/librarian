#include "book_tab.h"

BookTabForm::BookTabForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookTabForm),
    book(new Book),
    author(new Author)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);
    QDateTimeEdit *editionYear = this->findChild<QDateTimeEdit*>("editionYearEdit");
    editionYear->setMaximumDateTime(QDateTime::currentDateTime());

    QLineEdit *authorNameEdit = this->findChild<QLineEdit*>("authorNameEdit");
    AuthorNameValidator *validateAuthorName = new AuthorNameValidator(authorNameEdit);
    authorNameEdit->setValidator(validateAuthorName);
}

BookTabForm::~BookTabForm()
{
    delete ui;
}

void BookTabForm::on_okButton_clicked()
{
    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    if (nameEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Книга должна иметь название!\n"
                                 "Код ошибки: 1003"));
        return;
    }
    else
        book->setName(nameEdit->text());

    QLineEdit *authorNameEdit = this->findChild<QLineEdit*>("authorNameEdit");

    QSpinBox *countEdit = this->findChild<QSpinBox*>("countEdit");
    book->setCount(countEdit->value());

    QLineEdit *isbnEdit = this->findChild<QLineEdit*>("isbnEdit");
    if (!isbnEdit->text().isEmpty())
        book->setIsbn(isbnEdit->text());

    QLineEdit *udkEdit = this->findChild<QLineEdit*>("udkEdit");
    if (!udkEdit->text().isEmpty())
        book->setUdk(udkEdit->text());

    QLineEdit *bbkEdit = this->findChild<QLineEdit*>("bbkEdit");
    if (!bbkEdit->text().isEmpty())
        book->setBbk(bbkEdit->text());

    QLineEdit *publisherEdit = this->findChild<QLineEdit*>("publisherEdit");
    if (!publisherEdit->text().isEmpty())
        book->setPublisherName(publisherEdit->text());

    QSpinBox *editionEdit = this->findChild<QSpinBox*>("editionEdit");
    book->setEditionNum(editionEdit->value());

    QDateTimeEdit *editionYearEdit = this->findChild<QDateTimeEdit*>("editionYearEdit");
    book->setEditionYear(editionYearEdit->date());

    QStringList authors;
    if (authorNameEdit->text().isEmpty()) {
        qDebug() << "Warning: trying to insert book without authors";
        errorMessage(QObject::tr("Введите имена авторов, разделяя их запятой и пробелом.\n"
                                 "Если авторы неизвестны, введите \"Неизвестен\".\n"
                                 "Код ошибки: 1004"));
        return;
    } else {
        authors = authorNameEdit->text().split(", ");

        if (authors.size() > 10) {
            qDebug() << "Warning: trying to insert book wiht too many authors. Authors count = "
                        << authors.size();
            warningMessage(tr("Слишком много авторов. Максимальное количество - 10"));
            return;
        }

        foreach (QString authorName, authors)
            if (authorName.length() > 100) {
                qDebug() << "Warning: "<< authorName
                         << " is too long name. Size is " << authorName.length() << " symbols. "
                         << "Maximum length is 100 symbols";

                QString errorString;
                errorString = authorName;
                errorString += tr(" слишком длинное имя. Максимальная длина - 100 символов");
                warningMessage(errorString);
                return;
            }
    }

    QString confirmString = QObject::tr("Внести информацию о книге");
    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    if (book->exist(book->getName(), book->getIsbn())) {
        qDebug() << "Error: trying to add book that already exists in database";
        errorMessage(QObject::tr("Попытка добавить книгу, уже имеющуюся в базе данных.\n"
                                 "Код ошибки: 1201"));
        return;
    }

    book->setId(book->newBook());
    if (!book->getId())
        return;

    foreach (QString authorName,authors) {
        author->setName(authorName);
        author->setId(author->find());
        if (author->getId() == 0) {
            qDebug() << "Info: no author with name " << author->getName()
                     << " found. " << "Inserting new author";
            author->setId(author->add());
            if (author->getId() == 0)
                return;
        }
        if (!author->searchBook(book->getId())) {
            qDebug() << "Info: book " << book->getName() << " not found for author "
                     << author->getName() << ". Inserting new record";
            author->addBook(book->getId());
        }
    }

    QString message = QObject::tr("Книга №");
    message += QString::number(book->getId());
    message += " (";
    message += book->getName();
    message += QObject::tr(") добалена в базу данных");
    emit newBookAdded(message);
}
