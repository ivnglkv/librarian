#ifndef TABLE_FORM_H
#define TABLE_FORM_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QHash>
#include "messages.h"
#include "ui_table_form.h"
#include "libr_reports_gen_client.h"
#include "libr_print.h"
#include "report_types.h"

//#ifndef LIST_LINES
//#define LIST_LINES 5
//#endif // LIST_LINES

namespace Ui {
class TableForm;
}

class TableForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit TableForm(QString rowsCountQuery, QString getDataQuery,
                       QString btnsQuery, QString columnsName[], int columnsWidth[], int countCol,
                       QString title, QString errorStr, int maxRows,
                       ReportTypes type, uint id=0);
    ~TableForm();
    
private slots:
    void on_nextBtn_clicked();
    void on_prevBtn_clicked();
    void on_printBtn_clicked();
    void onPrintRequested(QString);

private:
    Ui::TableForm *ui;
    QSqlQueryModel *model;
    QSqlQuery query;
    QString bQuery;
    QString errStr;
    int totalCount;
    int curNum;
    int mRows;
    ReportTypes type;
    uint id;
};

#endif // TABLE_FORM_H
