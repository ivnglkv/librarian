#include "author.h"

Author::Author() : id(0),
    name("")
{
}

//    This function insert author with name given by Author::name in authors table.
//    Always remember to check if author with given name already exists by Author::find() function
//    or else you would be wondered why function returns 0.
//    If insert was successful, the id of inserted author is returned.
uint Author::add()
{
    if (name == "")
        return 0;

    QSqlQuery query;
    query.prepare("INSERT INTO authors VALUES (NULL, :name)");
    query.bindValue(":name", name);

    if (!query.exec()) {
        qDebug() << "Error: can't insert author with name " << name;
        QString errorStr = QObject::tr("Не удалось вставить автора по имени ");
        errorStr += name;
        errorStr += QObject::tr(".\nКод ошибки: 0001");
        errorMessage(errorStr);
        qDebug() << query.lastError();
        qDebug() << query.lastQuery();
        return 0;
    } else {
        return query.lastInsertId().toUInt();
    }
}

void Author::setId(const uint newId)
{
    id = newId;
}

uint Author::getId()
{
    return id;
}

void Author::setName(const QString &newName)
{
    name = newName;
}

QString Author::getName()
{
    return name;
}

//    This function search for an author with name given by Author::name value
//    in authors table.
//    If nothing was found, 0 is returned. Else the returned value is id of found author
uint Author::find()
{
    QSqlQuery query;
    query.prepare("SELECT id FROM authors WHERE name = :name");
    query.bindValue(":name", name);

    if (!query.exec())
        return 0;
    query.next();

    QSqlRecord rec = query.record();
    int idCol = rec.indexOf("id");

    int foundId = query.value(idCol).toInt();

    return foundId;
}

//    This function search for a row in books_authors table, containing
//    Author::id and Book::id (passed by bookId).
//    If Author::id == 0, then -1 is returned.
//    If Book::id == 0, or nothing was found, 0 is returned.
//    If search was succesfull, 1 is returned;
int Author::searchBook(const uint bookId)
{
    if ((id == 0) || (bookId == 0)) {
        return 0;
    }

    QSqlQuery query;
    query.prepare("SELECT idBook FROM books_authors "
                  "WHERE idBook = :idBook AND idAuthor = :idAuthor");
    query.bindValue(":idBook", bookId);
    query.bindValue(":idAuthor", id);

    if (!query.exec()) {
        return 0;
    } else {
        if (query.next()) {
            return 1;
        } else {
            return 0;
        }
    }
}

//    This function connects author with book through books_authors table.
//    Passed argument is id of book we want to associate with this author.
//    If Author::id == 0, then -1 is returned.
//    If Book::id <= 0, or nothing was found, 0 is returned.
//    If insert was succesfull, 1 is returned;
int Author::addBook(const uint bookId)
{
    if (id == 0) {
        return -1;
    } else if (bookId == 0) {
        return 0;
    }

    QSqlQuery query;
    query.prepare("INSERT INTO books_authors VALUES (:idBook, :idAuthor)");
    query.bindValue(":idBook", bookId);
    query.bindValue(":idAuthor", id);

    if (!query.exec()) {
        return 0;
    } else {
        return 1;
    }
}
