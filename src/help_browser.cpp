#include "help_browser.h"

HelpBrowser::HelpBrowser(QString startPage, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HelpBrowser)
{
    ui->setupUi(this);

    QPushButton *backwardBtn = this->findChild<QPushButton*>("backwardBtn");
    QPushButton *forwardBtn = this->findChild<QPushButton*>("forwardBtn");
    QPushButton *homeBtn = this->findChild<QPushButton*>("homeBtn");
    QTextBrowser *helpBrowser = this->findChild<QTextBrowser*>("helpBrowser");

    connect(backwardBtn, SIGNAL(clicked()),
            helpBrowser, SLOT(backward()));
    connect(forwardBtn, SIGNAL(clicked()),
            helpBrowser, SLOT(forward()));
    connect(homeBtn, SIGNAL(clicked()),
            helpBrowser, SLOT(home()));
    connect(helpBrowser, SIGNAL(backwardAvailable(bool)),
            backwardBtn, SLOT(setEnabled(bool)));
    connect(helpBrowser, SIGNAL(forwardAvailable(bool)),
            forwardBtn, SLOT(setEnabled(bool)));

    helpBrowser->setSearchPaths(QStringList() << "doc");
    helpBrowser->setSource(startPage);
}

HelpBrowser::~HelpBrowser()
{
    delete ui;
}
