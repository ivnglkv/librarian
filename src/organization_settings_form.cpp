#include "src/organization_settings_form.h"

OrganizationSettingsForm::OrganizationSettingsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrganizationSettingsForm)
{
    ui->setupUi(this);

    QSettings *settings = Librarian::librarianApp()->settings();
    QLineEdit *orgNameEdit = this->findChild<QLineEdit*>("orgNameEdit");
    settings->beginGroup("/Organization");
        orgNameEdit->setText(settings->value("/Name", "").toString());
    settings->endGroup();
}

void OrganizationSettingsForm::saveSettings() {
    QSettings *settings = Librarian::librarianApp()->settings();
    QLineEdit *orgNameEdit = this->findChild<QLineEdit*>("orgNameEdit");
    settings->setValue("/Organization/Name", orgNameEdit->text());
}

OrganizationSettingsForm::~OrganizationSettingsForm()
{
    delete ui;
}
