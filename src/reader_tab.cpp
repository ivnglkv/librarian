#include "reader_tab.h"

ReaderTabForm::ReaderTabForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReaderTabForm),
    reader(new Reader)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

    QDateEdit *passportDateEdit = this->findChild<QDateEdit*>("passportDateEdit");
    passportDateEdit->setMaximumDate(QDate::currentDate());

    QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
    otherDateEdit->setMaximumDate(QDate::currentDate());

    QLineEdit *fstNameEdit = this->findChild<QLineEdit*>("fstNameEdit");
    NameValidator *fstNameValidator = new NameValidator(fstNameEdit);
    fstNameEdit->setValidator(fstNameValidator);

    QLineEdit *secNameEdit = this->findChild<QLineEdit*>("secNameEdit");
    NameValidator *secNameValidator = new NameValidator(secNameEdit);
    secNameEdit->setValidator(secNameValidator);

    QLineEdit *patNameEdit = this->findChild<QLineEdit*>("patNameEdit");
    NameValidator *patNameValidator = new NameValidator(patNameEdit);
    patNameEdit->setValidator(patNameValidator);

    QLineEdit *passportSeriesEdit = this->findChild<QLineEdit*>("passportSeriesEdit");
    NumberValidator *passportSeriesValidator = new NumberValidator(passportSeriesEdit);
    passportSeriesEdit->setValidator(passportSeriesValidator);

    QLineEdit *passportNumberEdit = this->findChild<QLineEdit*>("passportNumberEdit");
    NumberValidator *passportNumberValidator = new NumberValidator(passportNumberEdit);
    passportNumberEdit->setValidator(passportNumberValidator);

    QLineEdit *homePhoneEdit = this->findChild<QLineEdit*>("homePhoneEdit");
    PhoneValidator *homePhoneValidator = new PhoneValidator(homePhoneEdit);
    homePhoneEdit->setValidator(homePhoneValidator);

    QLineEdit *mobPhoneEdit = this->findChild<QLineEdit*>("mobPhoneEdit");
    PhoneValidator *mobPhoneValidator = new PhoneValidator(mobPhoneEdit);
    mobPhoneEdit->setValidator(mobPhoneValidator);
}

ReaderTabForm::~ReaderTabForm()
{
    delete ui;
    delete reader;
}

void ReaderTabForm::on_okButton_clicked()
{
    QLineEdit *fstNameEdit = this->findChild<QLineEdit*>("fstNameEdit");
    if (fstNameEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите фамилию читателя.\n"
                                 "Код ошибки: 1022"));
        return;
    }
    reader->setFstName(fstNameEdit->text());

    QLineEdit *secNameEdit = this->findChild<QLineEdit*>("secNameEdit");
    if (secNameEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите имя читателя.\n"
                                 "Код ошибки: 1023"));
        return;
    }
    reader->setSecName(secNameEdit->text());

    QLineEdit *patNameEdit = this->findChild<QLineEdit*>("patNameEdit");
    reader->setPatName(patNameEdit->text());

    QLineEdit *passSerEdit = this->findChild<QLineEdit*>("passportSeriesEdit");
    QLineEdit *passNumEdit = this->findChild<QLineEdit*>("passportNumberEdit");
    QLineEdit *passWhoEdit = this->findChild<QLineEdit*>("passportWhoEdit");
    QDateEdit *passDateEdit = this->findChild<QDateEdit*>("passportDateEdit");
    QComboBox *otherBox = this->findChild<QComboBox*>("otherBox");

    reader->setPassSer(0);

    if (passSerEdit->text().isEmpty()
            && passNumEdit->text().isEmpty()
            && passWhoEdit->text().isEmpty()
            && otherBox->currentIndex() != 6) {
        errorMessage(QObject::tr("Необходимо ввести паспортные данные.\n"
                                 "Код ошибки: 1024"));
        return;
    } else if (passSerEdit->text().isEmpty()
                   && passNumEdit->text().isEmpty()
                   && passWhoEdit->text().isEmpty()
                   && (otherBox->currentIndex() == 6)) {
        reader->setPassSer(-1);

        reader->setOther(otherBox->currentText());

        QLineEdit *otherSerEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        if (otherSerEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите серию документа.\n"
                                     "Код ошибки: 1025"));
            return;
        } else {
            reader->setOtherSer(otherSerEdit->text().toInt());
        }

        QLineEdit *otherNumEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        if (otherNumEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите номер документа.\n"
                                     "Код ошибки: 1026"));
            return;
        } else {
            reader->setOtherNum(otherNumEdit->text().toInt());
        }

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        reader->setOtherWhen(otherDateEdit->date());

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        if (otherWhoEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите название организации выдавшей документ.\n"
                                     "Код ошибки: 1027"));
            return;
        } else {
            reader->setOtherWho(otherWhoEdit->text());
        }
    }

    if (reader->getPassSer() != -1) {
        if (passSerEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите серию паспорта.\n"
                                     "Код ошибки: 1028"));
            return;
        }
        reader->setPassSer(passSerEdit->text().toInt());

        if (passNumEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите номер паспорта.\n"
                                     "Код ошибки: 1029"));
            return;
        }
        reader->setPassNum(passNumEdit->text().toInt());

        if (passWhoEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите название организации, выдавшей паспорт.\n"
                                     "Код ошибки: 1030"));
            return;
        }
        reader->setPassWho(passWhoEdit->text());

        reader->setPassWhen(passDateEdit->date());
    }

    if ((otherBox->currentIndex() != 6)
            && (otherBox->currentIndex() != 0)) {
        reader->setOther(otherBox->currentText());

        QLineEdit *otherSerEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        if (otherSerEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите серию документа.\n"
                                     "Код ошибки: 1031"));
            return;
        }
        reader->setOtherSer(otherSerEdit->text().toInt());

        QLineEdit *otherNumEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        if (otherNumEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите номер документа.\n"
                                     "Код ошибки: 1032"));
            return;
        }
        reader->setOtherNum(otherNumEdit->text().toInt());

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        reader->setOtherWhen(otherDateEdit->date());

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        if (otherWhoEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите название организации выдавшей документ.\n"
                                     "Код ошибки: 1033"));
            return;
        }
        reader->setOtherWho(otherWhoEdit->text());
    } else if (otherBox->currentIndex() == 0)
        reader->setOther("");

    QLineEdit *areaEdit = this->findChild<QLineEdit*>("areaEdit");
    if (areaEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите область проживания.\n"
                                 "Код ошибки: 1034"));
        return;
    }
    reader->setArea(areaEdit->text());

    QLineEdit *cityEdit = this->findChild<QLineEdit*>("cityEdit");
    if (cityEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите название населенного пункта, в котором проживает читатель.\n"
                                 "Код ошибки: 1035"));
        return;
    }
    reader->setCity(cityEdit->text());

    QLineEdit *streetEdit = this->findChild<QLineEdit*>("streetEdit");
    if (streetEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите название улицы, на которой проживает читатель.\n"
                                 "Код ошибки: 1036"));
        return;
    }
    reader->setStreet(streetEdit->text());

    QLineEdit *houseEdit = this->findChild<QLineEdit*>("houseEdit");
    if (houseEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Введите номер дома, в котором проживает читатель.\n"
                                 "Код ошибки: 1037"));
        return;
    }
    reader->setHouse(houseEdit->text().toInt());

    QLineEdit *pavEdit = this->findChild<QLineEdit*>("pavEdit");
    reader->setPav(pavEdit->text());

    QLineEdit *roomEdit = this->findChild<QLineEdit*>("roomEdit");
    reader->setRoom(roomEdit->text().toInt());

    QLineEdit *homePhoneEdit = this->findChild<QLineEdit*>("homePhoneEdit");
    reader->setHomePhone(homePhoneEdit->text());

    QLineEdit *mobPhoneEdit = this->findChild<QLineEdit*>("mobPhoneEdit");
    reader->setMobilePhone(mobPhoneEdit->text());

    if (homePhoneEdit->text().isEmpty() && mobPhoneEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Необходимо ввести хотя бы один телефон.\n"
                                 "Код ошибки: 1038"));
        return;
    }

    QString confirmString = QObject::tr("Внести информацию о новом читателе?");
    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    int id = reader->addReader();
    if (id) {
        QString message = QObject::tr("Создан читатель №");
        message += QString::number(id);
        emit readerAdded(message);
    }
}

void ReaderTabForm::on_otherBox_currentIndexChanged(int index)
{
    if (index <= 0) {
        QLineEdit *otherSeriesEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        otherSeriesEdit->setEnabled(false);

        QLineEdit *otherNumberEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        otherNumberEdit->setEnabled(false);

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        otherDateEdit->setEnabled(false);

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        otherWhoEdit->setEnabled(false);
    }
    else {
        QLineEdit *otherSeriesEdit = this->findChild<QLineEdit*>("otherSeriesEdit");
        otherSeriesEdit->setEnabled(true);

        QLineEdit *otherNumberEdit = this->findChild<QLineEdit*>("otherNumberEdit");
        otherNumberEdit->setEnabled(true);

        QDateEdit *otherDateEdit = this->findChild<QDateEdit*>("otherDateEdit");
        otherDateEdit->setEnabled(true);

        QLineEdit *otherWhoEdit = this->findChild<QLineEdit*>("otherWhoEdit");
        otherWhoEdit->setEnabled(true);
    }
}
