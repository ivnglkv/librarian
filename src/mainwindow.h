#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "reader_tab.h"
#include "search_reader_tab.h"
#include "search_book_tab.h"
#include "book_profile.h"
#include "book_tab.h"
#include "book_edit_tab.h"
#include "reader_edit_tab.h"
#include "give_book_form.h"
#include "return_book_form.h"
#include "add_book_form.h"
#include "remove_book_form.h"
#include "login_form.h"
#include "settings_form.h"
#include "get_book_id_form.h"
#include "get_reader_id_form.h"
#include "remove_reader_form.h"
#include "librarian.h"
#include "help_browser.h"
#include "about_form.h"
#include "statistics_form.h"
#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_addReaderMenu_triggered();
    void on_changeReaderInfoMenu_triggered();
    void on_newBookMenu_triggered();
    void on_changeBookInfoMenu_triggered();
    void on_mainTabs_tabCloseRequested(int index);
    void on_searchBook_triggered();
    void on_searchReader_triggered();
    void editBook(int, QString &);
    void editBook(int);
    void editReader(int);
    void on_giveBookMenu_triggered();
    void on_returnBookMenu_triggered();
    void on_exitMenu_triggered();
    void on_aboutQtMenu_triggered();
    void on_removeBookMenu_triggered();
    void on_switchUserMenu_triggered();
    void on_configureMenu_triggered();
    void on_removeReaderMenu_triggered();
    void on_addBookMenu_triggered();
    void on_helpUserMenu_triggered();
    void on_aboutProgMenu_triggered();
    void on_helpAdminMenu_triggered();
    void on_statsMenu_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
