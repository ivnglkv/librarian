#include "network_settings_form.h"

NetworkSettingsForm::NetworkSettingsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NetworkSettingsForm)
{
    ui->setupUi(this);

    QSettings *settings = Librarian::librarianApp()->settings();
    QLineEdit *databaseEdit = this->findChild<QLineEdit*>("databaseEdit");
    QLineEdit *hostnameEdit = this->findChild<QLineEdit*>("hostnameEdit");
    QSpinBox *portEdit = this->findChild<QSpinBox*>("portEdit");
    settings->beginGroup("/Database");
        databaseEdit->setText(settings->value("/DbName", "library").toString());
        hostnameEdit->setText(settings->value("/Hostname", "localhost").toString());
        portEdit->setValue(settings->value("/Port", 3306).toInt());
    settings->endGroup();
}

void NetworkSettingsForm::saveSettings()
{
    QSettings *settings = Librarian::librarianApp()->settings();
    QLineEdit *databaseEdit = this->findChild<QLineEdit*>("databaseEdit");
    QLineEdit *hostnameEdit = this->findChild<QLineEdit*>("hostnameEdit");
    QSpinBox *portEdit = this->findChild<QSpinBox*>("portEdit");

    // If settings hasn't changed, we don't need to ask user for username and password again
    if (databaseEdit->text() != settings->value("/Database/DbName", "library").toString()
            || hostnameEdit->text() != settings->value("/Database/Hostname", "localhost").toString()
            || portEdit->text() != settings->value("/Database/Port", 3306).toString()) {
        settings->beginGroup("/Database");
            settings->setValue("/DbName", databaseEdit->text());
            settings->setValue("/Hostname", hostnameEdit->text());
            settings->setValue("/Port", portEdit->value());
        settings->endGroup();
        LoginForm *form = new LoginForm;
        form->show();
    }
}

NetworkSettingsForm::~NetworkSettingsForm()
{
    delete ui;
}
