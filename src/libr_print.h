#ifndef LIBR_PRINT_H
#define LIBR_PRINT_H

#include <QObject>
#include <QPrintPreviewDialog>
#include <QPrinter>
#include <QWebView>
#include <QDir>
#include <QTextCodec>
#include <QPixmap>
#include "librarian.h"

class LibrPrint : public QObject
{
    Q_OBJECT
public:
    explicit LibrPrint(QObject *parent = 0);
    ~LibrPrint();
    void printPreview(QString sHtml);
private:
    QWebView *webView;
    QPrintPreviewDialog *prevDlg;
};

#endif // LIBR_PRINT_H
