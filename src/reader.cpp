#include "reader.h"

Reader::Reader(): patName(""),
    other(""),
    pav(""),
    homePhone(""),
    mobilePhone("")
{
}

int Reader::addReader()
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QSqlQuery query;
    query.prepare("INSERT INTO readers "
                  "VALUES (NULL, :fstName, :secName, :patName, "
                  ":passSer, :passNum, :passWho, :passWhen, "
                  ":other, :otherSer, :otherNum, :otherWho, :otherWhen, "
                  ":area, :city, :street, :house, :pav, :room, "
                  ":homePhone, :mobilePhone);");

    query.bindValue(":fstName", fstName);
    query.bindValue(":secName", secName);
    query.bindValue(":patName", patName);

    // passSer is set to -1 if passport data fields weren't filled
    if (passSer == -1) {
        query.bindValue(":passSer", QVariant(QVariant::Int));
        query.bindValue(":passNum", QVariant(QVariant::Int));
        query.bindValue(":passWho", QVariant(QVariant::String));
        query.bindValue(":passWhen", QVariant(QVariant::Date));
    } else {
        query.bindValue(":passSer", passSer);
        query.bindValue(":passNum", passNum);
        query.bindValue(":passWho", passWho);
        query.bindValue(":passWhen", passWhen);
    }

    if (other == "") {
        query.bindValue(":other", QVariant(QVariant::String));
        query.bindValue(":otherSer", QVariant(QVariant::Int));
        query.bindValue(":otherNum", QVariant(QVariant::Int));
        query.bindValue(":otherWho", QVariant(QVariant::String));
        query.bindValue(":otherWhen", QVariant(QVariant::Date));
    } else {
        query.bindValue(":other", other);
        query.bindValue(":otherSer", otherSer);
        query.bindValue(":otherNum", otherNum);
        query.bindValue(":otherWho", otherWho);
        query.bindValue(":otherWhen", otherWhen);
    }

    query.bindValue(":area", area);
    query.bindValue(":city", city);
    query.bindValue(":street", street);
    query.bindValue(":house", house);

    if (pav == "")
        query.bindValue(":pav", QVariant(QVariant::String));
    else
        query.bindValue(":pav", pav);

    if (room == -1)
        query.bindValue(":room", QVariant(QVariant::Int));
    else
        query.bindValue(":room", room);

    if (homePhone == "")
        query.bindValue(":homePhone", QVariant(QVariant::String));
    else
        query.bindValue(":homePhone", homePhone);

    if (mobilePhone == "")
        query.bindValue(":mobilePhone", QVariant(QVariant::String));
    else
        query.bindValue(":mobilePhone", mobilePhone);

    if (!query.exec()) {
        qDebug() << "Unable to make insert reader operation";
        errorMessage(QObject::tr("Не удалось вставить запись в базу данных.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0006"));
        qDebug() << query.lastQuery();
        return 0;
    }
    return query.lastInsertId().toInt();
}

int Reader::updateReader()
{
    if (idReader <= 0)
        return 0;

    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QSqlQuery query;
    query.prepare("REPLACE INTO readers "
                  "VALUES (:id, :fstName, :secName, :patName, "
                  ":passSer, :passNum, :passWho, :passWhen, "
                  ":other, :otherSer, :otherNum, :otherWho, :otherWhen, "
                  ":area, :city, :street, :house, :pav, :room, "
                  ":homePhone, :mobilePhone);");

    query.bindValue(":id", idReader);

    query.bindValue(":fstName", fstName);
    query.bindValue(":secName", secName);
    query.bindValue(":patName", patName);

    // passSer is set to -1 if passport data fields weren't filled
    if (passSer == -1) {
        query.bindValue(":passSer", QVariant(QVariant::Int));
        query.bindValue(":passNum", QVariant(QVariant::Int));
        query.bindValue(":passWho", QVariant(QVariant::String));
        query.bindValue(":passWhen", QVariant(QVariant::Date));
    } else {
        query.bindValue(":passSer", passSer);
        query.bindValue(":passNum", passNum);
        query.bindValue(":passWho", passWho);
        query.bindValue(":passWhen", passWhen);
    }

    if (other == "") {
        query.bindValue(":other", QVariant(QVariant::String));
        query.bindValue(":otherSer", QVariant(QVariant::Int));
        query.bindValue(":otherNum", QVariant(QVariant::Int));
        query.bindValue(":otherWho", QVariant(QVariant::String));
        query.bindValue(":otherWhen", QVariant(QVariant::Date));
    } else {
        query.bindValue(":other", other);
        query.bindValue(":otherSer", otherSer);
        query.bindValue(":otherNum", otherNum);
        query.bindValue(":otherWho", otherWho);
        query.bindValue(":otherWhen", otherWhen);
    }

    query.bindValue(":area", area);
    query.bindValue(":city", city);
    query.bindValue(":street", street);
    query.bindValue(":house", house);

    if (pav == "")
        query.bindValue(":pav", QVariant(QVariant::String));
    else
        query.bindValue(":pav", pav);

    if (room == -1)
        query.bindValue(":room", QVariant(QVariant::Int));
    else
        query.bindValue(":room", room);

    if (homePhone == "")
        query.bindValue(":homePhone", QVariant(QVariant::String));
    else
        query.bindValue(":homePhone", homePhone);

    if (mobilePhone == "")
        query.bindValue(":mobilePhone", QVariant(QVariant::String));
    else
        query.bindValue(":mobilePhone", mobilePhone);

    if (!query.exec()) {
        qDebug() << "Unable to make replace reader operation";
        errorMessage(QObject::tr("Не удалось заменить запись в базе данных.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0402"));
        qDebug() << query.lastQuery();
        return 0;
    }

    return 1;
}

int Reader::removeReader()
{
    if (idReader <= 0) {
        qDebug() << "Error: bad id is given";
        errorMessage(QObject::tr("Неверный id читателя.\n"
                                 "Код ошибки: 2001"));
        return 0;
    }

    QSqlQuery query;
    query.prepare("DELETE FROM readers WHERE idReader = :id");
    query.bindValue(":id", idReader);
    if (!query.exec()) {
        qDebug() << "Error: can't delete reader" << idReader;
        qDebug() << query.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос на удаление профиля читателя.\n"
                                 "Код ошибки: 0601"));
        return 0;
    }
    query.prepare("DELETE FROM history WHERE idReader = :id");
    query.bindValue(":id", idReader);
    if (!query.exec()) {
        qDebug() << "Error: can't delete history for reader" << idReader;
        qDebug() << query.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос на удаление "
                                 "истории действий читателя.\n"
                                 "Код ошибки: 0602"));
        return 0;
    }
    return 1;
}

int Reader::giveBook(const uint idBook, const uint count)
{
    if ((idReader <= 0) || (idBook == 0)) {
        qDebug() << "Error: bad id's given";
        errorMessage(QObject::tr("Неверные id.\n"
                                 "Код ошибки: 2301"));
        return 0;
    }

    QSqlQuery selectBooksCount;
    selectBooksCount.prepare("SELECT count FROM books WHERE id = :id");
    selectBooksCount.bindValue(":id", idBook);
    if (!selectBooksCount.exec()) {
        qDebug() << "Error: can't retrieve number of books with id " << idBook;
        errorMessage(QObject::tr("Не удалось узнать количество имеющихся в наличии книг.\n"
                                 "Код ошибки: 0115"));
        return 0;
    }

    selectBooksCount.next();
    QSqlRecord booksCount = selectBooksCount.record();
    int countCol = booksCount.indexOf("count");
    uint havingCount = selectBooksCount.value(countCol).toUInt();
    if (havingCount < count) {
        qDebug() << "Warning: too many books were requested";
        QString message;
        message = QObject::tr("В библиотеке есть только ")
                  + QString::number(selectBooksCount.value(countCol).toUInt());
        switch (havingCount % 10) {
        case 1:
            if (havingCount != 11) {
                message += QObject::tr(" книга.");
                break;
            }
        case 2:
        case 3:
        case 4:
            if ((havingCount != 11)
                    && (havingCount != 12)
                    && (havingCount != 13)
                    && (havingCount != 14)) {
                message += QObject::tr(" книги.");
                break;
            }
        default:
            message += QObject::tr(" книг.");
            break;
        }
        message += QObject::tr("\nКод ошибки: 1101");
        errorMessage(message);
        return 0;
    }

    QSqlQuery searchPrevious;
    searchPrevious.prepare("SELECT * FROM taken_books WHERE "
                           "idBook = :idBook AND idReader = :idReader");
    searchPrevious.bindValue(":idBook", idBook);
    searchPrevious.bindValue(":idReader", idReader);
    if (!searchPrevious.exec()) {
        qDebug() << "Error: can't search for previous books gets";
        searchPrevious.lastError();
        errorMessage(QObject::tr("Не удалось получить информацию о предыдущих "
                                 "выдачах книги этому читателю.\n"
                                 "Код ошибки: 0116"));
        return 0;
    }

    searchPrevious.next();
    if (!searchPrevious.isValid()) {
        QSqlQuery insertGiving;
        insertGiving.prepare("INSERT INTO taken_books VALUES (:idBook, :idReader, :count)");
        insertGiving.bindValue(":idBook", idBook);
        insertGiving.bindValue(":idReader", idReader);
        insertGiving.bindValue(":count", count);

        if (!insertGiving.exec()) {
            qDebug() << "Error: can't give book " << idBook << " to reader " << idReader;
            insertGiving.lastError();
            errorMessage(QObject::tr("Не удалось выдать книгу читателю.\n"
                                     "Код ошибки: 0007"));
            return 0;
        }
    } else {
        QSqlQuery addGiving;
        addGiving.prepare("UPDATE taken_books SET count = :count "
                          "WHERE idBook = :idBook AND idReader = :idReader");
        addGiving.bindValue(":idBook", idBook);
        addGiving.bindValue(":idReader", idReader);
        QSqlRecord previous = searchPrevious.record();
        int takenCountCol = previous.indexOf("count");
        addGiving.bindValue(":count", searchPrevious.value(takenCountCol).toInt() + count);

        if (!addGiving.exec()) {
            qDebug() << "Error: can't give book " << idBook << " to reader " << idReader;
            addGiving.lastError();
            errorMessage(QObject::tr("Не удалось выдать книгу читателю.\n"
                                     "Код ошибки: 0008"));
            return 0;
        }
    }

    QSqlQuery addIntoHistory;
    addIntoHistory.prepare("INSERT INTO history VALUES "
                           "(NULL, :idBook, :idReader, :dateTime, :operation, :count)");
    addIntoHistory.bindValue(":idBook", idBook);
    addIntoHistory.bindValue(":idReader", idReader);
    addIntoHistory.bindValue(":dateTime", QDateTime::currentDateTime());
    addIntoHistory.bindValue(":operation", QObject::tr("Взятие"));
    addIntoHistory.bindValue(":count", count);

    if (!addIntoHistory.exec()) {
        qDebug() << "Error: can't write history log about giving book "
                 << idBook << " to reader " << idReader;
        addIntoHistory.lastError();
        errorMessage(QObject::tr("Не удалось сделать запись о выдаче книги читателю.\n"
                                 "Код ошибки: 0009"));
        return 0;
    }

    QSqlQuery deductBooksCount;
    deductBooksCount.prepare("UPDATE books SET count = :count WHERE id = :id");
    deductBooksCount.bindValue(":count", selectBooksCount.value(countCol).toUInt() - count);
    deductBooksCount.bindValue(":id", idBook);

    if (!deductBooksCount.exec()) {
        qDebug() << "Error: can't deduct books count";
        deductBooksCount.lastError();
        errorMessage(QObject::tr("Не удалось занести новое значение "
                                 "количества книг в библиотеке в базу данных.\n"
                                 "Код ошибки: 0303"));
        return 0;
    }

    return 1;
}

int Reader::returnBook(const uint idBook, const uint count)
{
    if ((idReader <= 0) || (idBook == 0) || (count <= 0)) {
        qDebug() << "Error: bad input data";
        errorMessage(QObject::tr("Неверные входные данные.\n"
                                 "Код ошибки: 1102"));
        return 0;
    }

    QSqlQuery query;
    query.prepare("SELECT count FROM taken_books "
                  "WHERE idBook = :idBook AND idReader = :idReader");
    query.bindValue(":idBook", idBook);
    query.bindValue(":idReader", idReader);

    if (!query.exec()) {
        qDebug() << "Error: can't retrive number of given books (id "
                 << idBook << ") to reader (id " << idReader << ")";
        query.lastError();
        errorMessage(QObject::tr("Не удалось выбрать количество взятых читателем книг.\n"
                                 "Код ошибки 0117"));
        return 0;
    }

    query.next();
    if (!query.isValid()) {
        qDebug() << "Warning: reader " << idReader << " never took books with id " << idBook;
        warningMessage(QObject::tr("Этот читатель не брал или уже вернул книги с таким id"));
        return 0;
    }

    QSqlRecord givenBooksCount = query.record();
    int countCol = givenBooksCount.indexOf("count");
    uint givenCount = givenBooksCount.value(countCol).toUInt();

    if (count > givenCount) {
        qDebug() << "Warning: trying to return too many books (" << givenCount
                 << ") from reader " << idReader << ". This reader took only "
                 << count << " books";
        QString message = QObject::tr("Неверное число возвращаемых книг: читатель ");
        message += QString::number(idReader);
        message += QObject::tr(" брал только ");
        message += QString::number(givenCount);
        switch (givenCount % 10) {
        case 1:
            if (givenCount != 11) {
                message += QObject::tr(" книгу.");
                break;
            }
        case 2:
        case 3:
        case 4:
            if ((givenCount != 11)
                    && (givenCount != 12)
                    && (givenCount != 13)
                    && (givenCount != 14)) {
                message += QObject::tr(" книги.");
                break;
            }
        default:
            message += QObject::tr(" книг.");
            break;
        }
        warningMessage(message);
        return 0;
    }

    uint onesGivenCount;

    onesGivenCount = query.value(countCol).toUInt();
    if (onesGivenCount > count) {
        QSqlQuery returnPart;
        returnPart.prepare("UPDATE taken_books SET count = :count "
                           "WHERE idBook = :idBook AND idReader = :idReader");
        returnPart.bindValue(":count", onesGivenCount - count);
        returnPart.bindValue(":idBook", idBook);
        returnPart.bindValue(":idReader", idReader);

        if (!returnPart.exec()) {
            qDebug() << "Error: can't update table taken books";
            returnPart.lastError();
            errorMessage(QObject::tr("Не удалось провести операцию возврата книг.\n"
                                     "Код ошибки: 0304"));
            return 0;
        }
    } else if (onesGivenCount == count) {
        QSqlQuery returnAll;
        returnAll.prepare("DELETE FROM taken_books WHERE "
                          "idBook = :idBook AND idReader = :idReader");
        returnAll.bindValue(":idBook", idBook);
        returnAll.bindValue(":idReader", idReader);

        if (!returnAll.exec()) {
            qDebug() << "Error: can't update table taken books";
            returnAll.lastError();
            errorMessage(QObject::tr("Не удалось провести операцию возврата книг.\n"
                                     "Код ошибки: 0603"));
            return 0;
        }
    }

    QSqlQuery addIntoHistory;
    addIntoHistory.prepare("INSERT INTO history VALUES "
                           "(NULL, :idBook, :idReader, :dateTime, :operation, :count)");
    addIntoHistory.bindValue(":idBook", idBook);
    addIntoHistory.bindValue(":idReader", idReader);
    addIntoHistory.bindValue(":dateTime", QDateTime::currentDateTime());
    addIntoHistory.bindValue(":operation", QObject::tr("Возвращение"));
    addIntoHistory.bindValue(":count", count);

    if (!addIntoHistory.exec()) {
        qDebug() << "Error: can't write history log about giving book "
                 << idBook << " to reader " << idReader;
        addIntoHistory.lastError();
        errorMessage(QObject::tr("Не удалось сделать запись о возвращении книг читателем.\n"
                                 "Код ошибки: 0010"));
        return 0;
    }

    QSqlQuery selectBooksCount;
    selectBooksCount.prepare("SELECT count FROM books WHERE id = :id");
    selectBooksCount.bindValue(":id", idBook);
    if (!selectBooksCount.exec()) {
        qDebug() << "Error: can't retrieve number of books with id " << idBook;
        errorMessage(QObject::tr("Не удалось узнать количество имеющихся в наличии книг.\n"
                                 "Код ошибки: 0118"));
        return 0;
    }

    selectBooksCount.next();
    QSqlRecord booksCount = selectBooksCount.record();
    countCol = booksCount.indexOf("count");

    QSqlQuery deductBooksCount;
    deductBooksCount.prepare("UPDATE books SET count = :count WHERE id = :id");
    deductBooksCount.bindValue(":count", selectBooksCount.value(countCol).toUInt() + count);
    deductBooksCount.bindValue(":id", idBook);

    if (!deductBooksCount.exec()) {
        qDebug() << "Error: can't deduct books count";
        deductBooksCount.lastError();
        errorMessage(QObject::tr("Не удалось занести новое значение "
                                 "количества книг в библиотеке в базу данных.\n"
                                 "Код ошибки: 0305"));
        return 0;
    }

    return 1;
}

int Reader::getIdReader()
{
    return idReader;
}

//    This function checks existense of reader with given id.
//    If such reader exists, new value of idReader is equal to newIdReader
//    (and idReader is returned).
//    Else, if reader with given id doesn't exists, or query wasn't executed, 0 is returned
int Reader::setIdReader(const int newIdReader)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM readers WHERE idReader = :idReader");
    query.bindValue(":idReader", newIdReader);
    if (!query.exec()) {
        qDebug() << "Error: can't select reader " << newIdReader;
        qDebug() << query.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос на поиск читателя в базе данных.\n"
                                 "Код ошибки: 0119"));
        return 0;
    }

    query.next();
    if (!query.isValid()) {
        qDebug() << "Warning: reader " << newIdReader << " wasn't found";
        warningMessage(QObject::tr("В базе данных нет читателя с таким id"));
        return 0;
    }

    idReader = newIdReader;
    return idReader;
}

QString Reader::getFstName()
{
    if (fstName == "") {
        QSqlQuery query;
        query.prepare("SELECT fstName FROM readers WHERE idReader = :id");
        query.bindValue(":id", idReader);
        if (!query.exec()) {
            qDebug() << "Error: can't select reader" << idReader;
            qDebug() << query.lastError();
            return "";
        }
        query.next();
        if (!query.isValid()) {
            qDebug() << "Warning: reader" << idReader << "wasn't found";
            return "";
        }
        QSqlRecord idRecord = query.record();
        int fstNameCol = idRecord.indexOf("fstName");
        return query.value(fstNameCol).toString();
    }
    return fstName;
}

void Reader::setFstName(QString newFstName)
{
    fstName = newFstName;
}

QString Reader::getSecName()
{
    if (secName == "") {
        QSqlQuery query;
        query.prepare("SELECT secName FROM readers WHERE idReader = :id");
        query.bindValue(":id", idReader);
        if (!query.exec()) {
            qDebug() << "Error: can't select reader" << idReader;
            qDebug() << query.lastError();
            return "";
        }
        query.next();
        if (!query.isValid()) {
            qDebug() << "Warning: reader" << idReader << "wasn't found";
            return "";
        }
        QSqlRecord idRecord = query.record();
        int secNameCol = idRecord.indexOf("secName");
        return query.value(secNameCol).toString();
    }
    return secName;
}

void Reader::setSecName(QString newSecName)
{
    secName = newSecName;
}

QString Reader::getPatName()
{
    if (patName == "") {
        QSqlQuery query;
        query.prepare("SELECT patName FROM readers WHERE idReader = :id");
        query.bindValue(":id", idReader);
        if (!query.exec()) {
            qDebug() << "Error: can't select reader" << idReader;
            qDebug() << query.lastError();
            return "";
        }
        query.next();
        if (!query.isValid()) {
            qDebug() << "Warning: reader" << idReader << "wasn't found";
            return "";
        }
        QSqlRecord idRecord = query.record();
        int patNameCol = idRecord.indexOf("patName");
        return query.value(patNameCol).toString();
    }
    return patName;
}

void Reader::setPatName(QString newPatName)
{
    patName = newPatName;
}

int Reader::getPassSer()
{
    return passSer;
}

void Reader::setPassSer(const int newPassSer)
{
    passSer = newPassSer;
}

int Reader::getPassNum()
{
    return passNum;
}

void Reader::setPassNum(const int newPassNum)
{
    passNum = newPassNum;
}

QString Reader::getPassWho()
{
    return passWho;
}

void Reader::setPassWho(QString newPassWho)
{
    passWho = newPassWho;
}

QDate Reader::getPassWhen()
{
    return passWhen;
}

void Reader::setPassWhen(QDate newPassWhen)
{
    passWhen = newPassWhen;
}

QString Reader::getOther()
{
    return other;
}

void Reader::setOther(QString newOther)
{
    other = newOther;
}

int Reader::getOtherSer()
{
    return otherSer;
}

void Reader::setOtherSer(const int newOtherSer)
{
    otherSer = newOtherSer;
}

int Reader::getOtherNum()
{
    return otherNum;
}

void Reader::setOtherNum(const int newOtherNum)
{
    otherNum = newOtherNum;
}

QString Reader::getOtherWho()
{
    return otherWho;
}

void Reader::setOtherWho(QString newOtherWho)
{
    otherWho = newOtherWho;
}

QDate Reader::getOtherWhen()
{
    return otherWhen;
}

void Reader::setOtherWhen(QDate newOtherWhen)
{
    otherWhen = newOtherWhen;
}

QString Reader::getArea()
{
    return area;
}

void Reader::setArea(QString newArea)
{
    area = newArea;
}

QString Reader::getCity()
{
    return city;
}

void Reader::setCity(QString newCity)
{
    city = newCity;
}

QString Reader::getStreet()
{
    return street;
}

void Reader::setStreet(QString newStreet)
{
    street = newStreet;
}

int Reader::getHouse()
{
    return house;
}

void Reader::setHouse(const int newHouse)
{
    house = newHouse;
}

QString Reader::getPav()
{
    return pav;
}

void Reader::setPav(QString newPav)
{
    pav = newPav;
}

int Reader::getRoom()
{
    return room;
}

void Reader::setRoom(const int newRoom)
{
    room = newRoom;
}

QString Reader::getHomePhone()
{
    return homePhone;
}

void Reader::setHomePhone(QString newHomePhone)
{
    homePhone = newHomePhone;
}

QString Reader::getMobilePhone()
{
    return mobilePhone;
}

void Reader::setMobilePhone(QString newMobilePhone)
{
    mobilePhone = newMobilePhone;
}
