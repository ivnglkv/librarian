#ifndef GET_READER_ID_FORM_H
#define GET_READER_ID_FORM_H

#include <QKeyEvent>
#include <QSqlQuery>

#include "messages.h"
#include "ui_get_reader_id_form.h"

namespace Ui {
class GetReaderIdForm;
}

class GetReaderIdForm : public QWidget
{
    Q_OBJECT
public:
    explicit GetReaderIdForm(QWidget *parent = 0);
    ~GetReaderIdForm();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::GetReaderIdForm *ui;

    void keyPressEvent(QKeyEvent *);

signals:
    void editReaderRequested(int);
};

#endif // GET_READER_ID_FORM_H
