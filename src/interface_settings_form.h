#ifndef INTERFACE_SETTINGS_FORM_H
#define INTERFACE_SETTINGS_FORM_H

#include <QStyleFactory>
#include <QSettings>
#include <QApplication>

#include "librarian.h"
#include "ui_interface_settings_form.h"

namespace Ui {
class InterfaceSettingsForm;
}

class InterfaceSettingsForm : public QWidget
{
    Q_OBJECT
public:
    explicit InterfaceSettingsForm(QWidget *parent = 0);
    ~InterfaceSettingsForm();

public slots:
    void saveSettings();
    void rejectSettings();
    
private slots:
    void on_styleBox_currentIndexChanged(const QString &style);

private:
    Ui::InterfaceSettingsForm *ui;
};

#endif // INTERFACE_SETTINGS_FORM_H
