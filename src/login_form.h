#ifndef LOGIN_FORM_H
#define LOGIN_FORM_H

#include <QWidget>
#include <QSettings>

#include "librarian.h"
#include "messages.h"
#include "db_connection.h"
#include "ui_login_form.h"

namespace Ui {
class LoginForm;
}

class LoginForm : public QWidget
{
    Q_OBJECT
public:
    explicit LoginForm(QWidget *parent = 0);
    ~LoginForm();
    void keyPressEvent(QKeyEvent *);

private slots:
    void on_okButton_clicked();

private:
    Ui::LoginForm *ui;
};

#endif // LOGIN_FORM_H
