#ifndef GET_BOOK_ID_FORM_H
#define GET_BOOK_ID_FORM_H

#include <QKeyEvent>
#include <QSqlQuery>

#include "messages.h"
#include "ui_get_book_id_form.h"

namespace Ui {
class GetBookIdForm;
}

class GetBookIdForm : public QWidget
{
    Q_OBJECT
public:
    explicit GetBookIdForm(QWidget *parent = 0);
    ~GetBookIdForm();
    
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::GetBookIdForm *ui;
    void keyPressEvent(QKeyEvent *);

signals:
    void editBookRequested(int);
};

#endif // GET_BOOK_ID_FORM_H
