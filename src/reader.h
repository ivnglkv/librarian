#ifndef READER_H
#define READER_H

#include <QString>
#include <QDate>
#include <QtSql>
#include <QVector>

#include "messages.h"

class Reader
{
public:
    Reader();
    int addReader();
    int updateReader();
    int removeReader();
    int giveBook(const uint, const uint);
    int returnBook(const uint, const uint);

    int getIdReader();
    int setIdReader(const int);

    QString getFstName();
    void setFstName(QString);

    QString getSecName();
    void setSecName(QString);

    QString getPatName();
    void setPatName(QString);

    int getPassSer();
    void setPassSer(const int);

    int getPassNum();
    void setPassNum(const int);

    QString getPassWho();
    void setPassWho(QString);

    QDate getPassWhen();
    void setPassWhen(QDate);

    QString getOther();
    void setOther(QString);

    int getOtherSer();
    void setOtherSer(const int);

    int getOtherNum();
    void setOtherNum(const int);

    QString getOtherWho();
    void setOtherWho(QString);

    QDate getOtherWhen();
    void setOtherWhen(QDate);

    QString getArea();
    void setArea(QString);

    QString getCity();
    void setCity(QString);

    QString getStreet();
    void setStreet(QString);

    int getHouse();
    void setHouse(const int);

    QString getPav();
    void setPav(QString);

    int getRoom();
    void setRoom(const int);

    QString getHomePhone();
    void setHomePhone(QString);

    QString getMobilePhone();
    void setMobilePhone(QString);
private:
    int idReader;
    QString fstName;
    QString secName;
    QString patName;
    int passSer;
    int passNum;
    QString passWho;
    QDate passWhen;
    QString other;
    int otherSer;
    int otherNum;
    QString otherWho;
    QDate otherWhen;
    QString area;
    QString city;
    QString street;
    int house;
    QString pav;
    int room;
    QString homePhone;
    QString mobilePhone;
};

#endif // READER_H
