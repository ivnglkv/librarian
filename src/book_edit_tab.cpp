#include "book_edit_tab.h"

BookEditTabForm::BookEditTabForm(int id, QString &authorsStr, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookEditTabForm),
    book(new Book),
    author(new Author)
{
    ui->setupUi(this);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QDateTimeEdit *editionYear = this->findChild<QDateTimeEdit*>("editionYearEdit");
    editionYear->setMaximumDateTime(QDateTime::currentDateTime());

    QLineEdit *authorNameEdit = this->findChild<QLineEdit*>("authorNameEdit");
    AuthorNameValidator *validateAuthorName = new AuthorNameValidator(authorNameEdit);
    authorNameEdit->setValidator(validateAuthorName);

//    Common group
    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    QSpinBox *countEdit = this->findChild<QSpinBox*>("countEdit");

//    Identificators group
    QLineEdit *isbnEdit = this->findChild<QLineEdit*>("isbnEdit");
    QLineEdit *udkEdit = this->findChild<QLineEdit*>("udkEdit");
    QLineEdit *bbkEdit = this->findChild<QLineEdit*>("bbkEdit");

//    Edition group
    QLineEdit *publisherEdit = this->findChild<QLineEdit*>("publisherEdit");
    QSpinBox *editionEdit = this->findChild<QSpinBox*>("editionEdit");
    QDateEdit *editionYearEdit = this->findChild<QDateEdit*>("editionYearEdit");

    QSqlQuery getBook;
    getBook.prepare("SELECT * FROM books WHERE id = :id");
    getBook.bindValue(":id", id);

    if (!getBook.exec()) {
        qDebug() << "Error: unable to make select reader operation";
        errorMessage(QObject::tr("Не удалось найти книгу в базе данных.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0105"));
        return;
    }

    QSqlRecord bookRec = getBook.record();
    int nameCol = bookRec.indexOf("name");
    int isbnCol = bookRec.indexOf("isbn");
    int udkCol = bookRec.indexOf("udk");
    int bbkCol = bookRec.indexOf("bbk");
    int publisherNameCol = bookRec.indexOf("publisherName");
    int editionNumCol = bookRec.indexOf("editionNum");
    int editionYearCol = bookRec.indexOf("editionYear");
    int countCol = bookRec.indexOf("count");

    getBook.next();
    book->setId(id);
    book->setName(getBook.value(nameCol).toString());
    book->setIsbn(getBook.value(isbnCol).toString());
    book->setUdk(getBook.value(udkCol).toString());
    book->setBbk(getBook.value(bbkCol).toString());
    book->setPublisherName(getBook.value(publisherNameCol).toString());
    book->setEditionNum(getBook.value(editionNumCol).toInt());
    book->setEditionYear(getBook.value(editionYearCol).toDate());
    book->setCount(getBook.value(countCol).toInt());

    nameEdit->setText(book->getName());
    isbnEdit->setText(book->getIsbn());
    udkEdit->setText(book->getUdk());
    bbkEdit->setText(book->getBbk());
    publisherEdit->setText(book->getPublisherName());
    editionEdit->setValue(book->getEditionNum());
    editionYearEdit->setDate(book->getEditionYear());
    countEdit->setValue(book->getCount());
    countEdit->setEnabled(false);

    authorNameEdit->setText(authorsStr);

    QStringList oldAuthorsNamesList = authorsStr.split(", ");

    foreach (QString str, oldAuthorsNamesList) {
        author->setName(str);
        author->setId(author->find());
        if (!author->getId()) {
            errorMessage(QObject::tr("В функцию были переданы неправильные параметры.\n"
                                     "Код ошибки: 0901"));
            qDebug() << "Error: no entry found for author name " << str;
            return;
        } else {
            oldAuthorsIdList << author->getId();
        }
    }
}

BookEditTabForm::BookEditTabForm(int id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BookEditTabForm),
    book(new Book),
    author(new Author)
{
    ui->setupUi(this);
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QDateTimeEdit *editionYear = this->findChild<QDateTimeEdit*>("editionYearEdit");
    editionYear->setMaximumDateTime(QDateTime::currentDateTime());

    QLineEdit *authorNameEdit = this->findChild<QLineEdit*>("authorNameEdit");
    AuthorNameValidator *validateAuthorName = new AuthorNameValidator(authorNameEdit);
    authorNameEdit->setValidator(validateAuthorName);

//    Common group
    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    QSpinBox *countEdit = this->findChild<QSpinBox*>("countEdit");

//    Identificators group
    QLineEdit *isbnEdit = this->findChild<QLineEdit*>("isbnEdit");
    QLineEdit *udkEdit = this->findChild<QLineEdit*>("udkEdit");
    QLineEdit *bbkEdit = this->findChild<QLineEdit*>("bbkEdit");

//    Edition group
    QLineEdit *publisherEdit = this->findChild<QLineEdit*>("publisherEdit");
    QSpinBox *editionEdit = this->findChild<QSpinBox*>("editionEdit");
    QDateEdit *editionYearEdit = this->findChild<QDateEdit*>("editionYearEdit");

    QSqlQuery getBook;
    getBook.prepare("SELECT * FROM books WHERE id = :id");
    getBook.bindValue(":id", id);

    if (!getBook.exec()) {
        qDebug() << "Error: unable to make select reader operation";
        errorMessage(QObject::tr("Не удалось найти книгу в базе данных.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0106"));
        return;
    }

    QSqlRecord bookRec = getBook.record();
    int nameCol = bookRec.indexOf("name");
    int isbnCol = bookRec.indexOf("isbn");
    int udkCol = bookRec.indexOf("udk");
    int bbkCol = bookRec.indexOf("bbk");
    int publisherNameCol = bookRec.indexOf("publisherName");
    int editionNumCol = bookRec.indexOf("editionNum");
    int editionYearCol = bookRec.indexOf("editionYear");
    int countCol = bookRec.indexOf("count");

    getBook.next();
    book->setId(id);
    book->setName(getBook.value(nameCol).toString());
    book->setIsbn(getBook.value(isbnCol).toString());
    book->setUdk(getBook.value(udkCol).toString());
    book->setBbk(getBook.value(bbkCol).toString());
    book->setPublisherName(getBook.value(publisherNameCol).toString());
    book->setEditionNum(getBook.value(editionNumCol).toInt());
    book->setEditionYear(getBook.value(editionYearCol).toDate());
    book->setCount(getBook.value(countCol).toInt());

    nameEdit->setText(book->getName());
    isbnEdit->setText(book->getIsbn());
    udkEdit->setText(book->getUdk());
    bbkEdit->setText(book->getBbk());
    publisherEdit->setText(book->getPublisherName());
    editionEdit->setValue(book->getEditionNum());
    editionYearEdit->setDate(book->getEditionYear());
    countEdit->setValue(book->getCount());
    countEdit->setEnabled(false);

    QSqlQuery getAuthors;
    getAuthors.prepare("SELECT * FROM authors WHERE id IN "
                       "(SELECT idAuthor FROM books_authors WHERE idBook = :id)");
    getAuthors.bindValue(":id", id);
    if (!getAuthors.exec()) {
        errorMessage(QObject::tr("Не удалось выполнить запрос на поиск авторов книги.\n"
                                 "Код ошибки: 0107"));
        return;
    }
    getAuthors.next();
    if (!getAuthors.isValid()) {
        warningMessage(QObject::tr("Не найдено ни одного автора"));
        return;
    }

    QSqlRecord authorsRec = getAuthors.record();
    int authorNameCol = authorsRec.indexOf("name");
    int authorIdCol = authorsRec.indexOf("id");
    do {
        authorNameEdit->setText(authorNameEdit->text()
                                + getAuthors.value(authorNameCol).toString());
        oldAuthorsIdList << getAuthors.value(authorIdCol).toUInt();
        if (getAuthors.next()) {
            authorNameEdit->setText(authorNameEdit->text()
                                    + ", ");
        } else {
            break;
        }
    } while (1);
}

BookEditTabForm::~BookEditTabForm()
{
    delete ui;
    delete book;
    delete author;
}

void BookEditTabForm::on_okButton_clicked()
{
    QString confirmString = QObject::tr("Обновить информацию о книге?");
    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    if (nameEdit->text().isEmpty()) {
        errorMessage(QObject::tr("Книга должна иметь название!\n"
                                 "Код ошибки: 1001"));
        return;
    }
    else
        book->setName(nameEdit->text());

    QLineEdit *isbnEdit = this->findChild<QLineEdit*>("isbnEdit");
    if (!isbnEdit->text().isEmpty())
        book->setIsbn(isbnEdit->text());

    QLineEdit *udkEdit = this->findChild<QLineEdit*>("udkEdit");
    if (!udkEdit->text().isEmpty())
        book->setUdk(udkEdit->text());

    QLineEdit *bbkEdit = this->findChild<QLineEdit*>("bbkEdit");
    if (!bbkEdit->text().isEmpty())
        book->setBbk(bbkEdit->text());

    QLineEdit *publisherEdit = this->findChild<QLineEdit*>("publisherEdit");
    if (!publisherEdit->text().isEmpty())
        book->setPublisherName(publisherEdit->text());

    QSpinBox *editionEdit = this->findChild<QSpinBox*>("editionEdit");
    book->setEditionNum(editionEdit->value());

    QDateTimeEdit *editionYearEdit = this->findChild<QDateTimeEdit*>("editionYearEdit");
    book->setEditionYear(editionYearEdit->date());

    if (!book->updateBook())
        return;

    QLineEdit *authorNameEdit = this->findChild<QLineEdit*>("authorNameEdit");
    QStringList authorsList;
    if (authorNameEdit->text().isEmpty()) {
        qDebug() << "Warning: trying to insert book without authors";
        errorMessage(QObject::tr("Введите имена авторов, разделяя их запятой и пробелом.\n"
                                 "Если авторы неизвестны, введите \"Неизвестен\".\n"
                                 "Код ошибки: 1002"));
        return;
    } else {
        authorsList = authorNameEdit->text().split(", ");

        if (authorsList.size() > 10) {
            qDebug() << "Warning: trying to insert book wiht too many authors. Authors count = "
                        << authorsList.size();
            warningMessage(tr("Слишком много авторов. Максимальное количество - 10"));
            return;
        }

        foreach (QString authorName, authorsList)
            if (authorName.length() > 100) {
                qDebug() << "Warning: "<< authorName
                         << " is too long name. Size is " << authorName.length() << " symbols. "
                         << "Maximum length is 100 symbols";

                QString errorString;
                errorString = authorName;
                errorString += tr(" слишком длинное имя. Максимальная длина - 100 символов");
                warningMessage(errorString);
                return;
            }
    }

    foreach (QString authorName, authorsList) {
        author->setName(authorName);
        author->setId(author->find());
        if (!author->getId()) {
            qDebug() << "Info: adding author";
            author->setId(author->add());
            if (!author->getId()) {
                qDebug() << "Error: author " << author->getName() <<"wasn't inserted";
                errorMessage(QObject::tr("Не удалось добавить автора.\n"
                                         "Код ошибки: 0003"));
                continue;
            }
            if (author->addBook(book->getId()) != 1) {
                qDebug() << "Error: book " << book->getId() << "were not inserted for author "
                         << author->getId();
                errorMessage(QObject::tr("Не удалось связать автора с книгой.\n"
                                         "Код ошибки: 0004"));
                continue;
            }
        } else if (!author->searchBook(book->getId())) {
            qDebug() << "Info: adding book for author";
            if (author->addBook(book->getId()) != 1) {
                qDebug() << "Error: book " << book->getId() << "were not inserted for author "
                         << author->getId();
                errorMessage(QObject::tr("Не удалось связать автора с книгой.\n"
                                         "Код ошибки: 0005"));
                continue;
            }
        } else {
            qDebug() << "Info: author " << author->getId() <<
                        " already linked with book " << book->getId();
        }
        newAuthorsIdList << author->getId();
    }

    foreach (uint oldId, oldAuthorsIdList)
        if (!newAuthorsIdList.contains(oldId)) {
            qDebug() << "Info: removing link for book " << book->getId()
                     << " with author " << oldId;
            QSqlQuery removeAuthor;
            removeAuthor.prepare("DELETE FROM books_authors WHERE idAuthor = :idAuthor "
                                 "AND idBook = :idBook");
            removeAuthor.bindValue(":idAuthor", oldId);
            removeAuthor.bindValue(":idBook", book->getId());

            if (!removeAuthor.exec()) {
                qDebug() << "Error: unable to delete book " << book->getId()
                         << " for author " << oldId;
                return;
            }
        }

    oldAuthorsIdList = newAuthorsIdList;
    newAuthorsIdList.clear();

    QString message = QObject::tr("Обновлена информация о книге №");
    message += QString::number(book->getId())
            + " ("
            + book->getName()
            + ")";
    emit bookUpdated(message);
}
