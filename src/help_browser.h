#ifndef HELP_BROWSER_H
#define HELP_BROWSER_H

#include <QWidget>
#include <QPushButton>
#include <QTextBrowser>
#include <QString>
#include <QStringList>
#include <QDir>
#include <QDebug>

#include "ui_help_browser.h"

namespace Ui {
class HelpBrowser;
}

class HelpBrowser : public QWidget
{
    Q_OBJECT
    
public:
    explicit HelpBrowser(QString, QWidget *parent = 0);
    ~HelpBrowser();
    
private:
    Ui::HelpBrowser *ui;
};

#endif // HELP_BROWSER_H
