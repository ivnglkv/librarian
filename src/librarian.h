#ifndef LIBRARIAN_H
#define LIBRARIAN_H

#include <QApplication>
#include <QSettings>

class Librarian : public QApplication
{
Q_OBJECT
public:
    Librarian(int &argc,
              char **argv,
              const QString& strOrg,
              const QString& strAppName);
    static Librarian* librarianApp()
    {
        return (Librarian*)qApp;
    }
    QSettings *settings();

private:
    QSettings *pSettings;
};

#endif // LIBRARIAN_H
