#include "librarian.h"

Librarian::Librarian(int &argc,
                     char **argv,
                     const QString &strOrg,
                     const QString &strAppName) : QApplication(argc, argv),
    pSettings(0)
{
    setOrganizationName(strOrg);
    setApplicationName(strAppName);

    pSettings = new QSettings(strOrg, strAppName, this);
}

QSettings* Librarian::settings()
{
    return pSettings;
}
