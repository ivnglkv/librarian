#include "db_connection.h"

bool createConnection(QString &name, QString &pass)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    QSettings *settings = Librarian::librarianApp()->settings();
    settings->beginGroup("/Database");
        db.setHostName(settings->value("/Hostname", "localhost").toString());
        db.setPort(settings->value("/Port", 3306).toInt());
        db.setDatabaseName(settings->value("/DbName", "library").toString());
        db.setUserName(name);
        db.setPassword(pass);
    settings->endGroup();

    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError();
        return false;
    }

    return true;
}
