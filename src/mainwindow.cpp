#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

    QSettings *settings = Librarian::librarianApp()->settings();
    QStyle *pStyle = QStyleFactory::create(settings->value("/Interface/Style", "Gtk").toString());
    QApplication::setStyle(pStyle);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addReaderMenu_triggered()
{
    ReaderTabForm *readerTab = new ReaderTabForm;
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    int curIndex = mainTabs->addTab(readerTab,
                                    QIcon(QPixmap(":icons/user_add.png")),
                                    tr("Добавление читателя"));
    mainTabs->setCurrentIndex(curIndex);

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(readerTab, SIGNAL(readerAdded(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_changeReaderInfoMenu_triggered()
{
    GetReaderIdForm *getIdForm = new GetReaderIdForm;
    QObject::connect(getIdForm, SIGNAL(editReaderRequested(int)),
                     this, SLOT(editReader(int)));
    getIdForm->show();
}

void MainWindow::on_newBookMenu_triggered()
{
    BookTabForm *bookTab = new BookTabForm;
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    int curIndex = mainTabs->addTab(bookTab,
                                    QIcon(QPixmap(":icons/book_add.png")),
                                    tr("Новая книга"));
    mainTabs->setCurrentIndex(curIndex);

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(bookTab, SIGNAL(newBookAdded(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_changeBookInfoMenu_triggered()
{
    GetBookIdForm *getIdForm = new GetBookIdForm;
    QObject::connect(getIdForm, SIGNAL(editBookRequested(int)),
                     this, SLOT(editBook(int)));
    getIdForm->show();
}

void MainWindow::on_mainTabs_tabCloseRequested(int index)
{
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    mainTabs->removeTab(index);
}

void MainWindow::on_searchBook_triggered()
{
    SearchBookTabForm *bookSearch = new SearchBookTabForm();
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    int curIndex = mainTabs->addTab(bookSearch,
                                    QIcon(QPixmap(":icons/book_search.png")),
                                    tr("Поиск книги"));
    mainTabs->setCurrentIndex(curIndex);
    QObject::connect(bookSearch, SIGNAL(editBookRedirect(int, QString &)),
                     this, SLOT(editBook(int, QString &)));

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(bookSearch, SIGNAL(booksFound(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_searchReader_triggered()
{
    SearchReaderTabForm *readerSearch = new SearchReaderTabForm();
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    int curIndex = mainTabs->addTab(readerSearch,
                                    QIcon(QPixmap(":icons/user_search.png")),
                                    tr("Поиск читателя"));
    mainTabs->setCurrentIndex(curIndex);
    QObject::connect(readerSearch, SIGNAL(editBook(int)),
                     this, SLOT(editReader(int)));

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(readerSearch, SIGNAL(readersFound(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::editBook(int id, QString &authors)
{
    BookEditTabForm *bookTab = new BookEditTabForm(id, authors);
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    QString header = tr("Изменение книги №");
    header += QString::number(id);
    int curIndex = mainTabs->addTab(bookTab,
                                    QIcon(QPixmap(":icons/book_cnange_info.png")),
                                    header);
    mainTabs->setCurrentIndex(curIndex);

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(bookTab, SIGNAL(bookUpdated(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::editBook(int id)
{
    BookEditTabForm *bookTab = new BookEditTabForm(id);
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    QString header = tr("Изменение книги №");
    header += QString::number(id);
    int curIndex = mainTabs->addTab(bookTab,
                                    QIcon(QPixmap(":icons/book_cnange_info.png")),
                                    header);
    mainTabs->setCurrentIndex(curIndex);

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(bookTab, SIGNAL(bookUpdated(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::editReader(int id)
{
    ReaderEditTabForm *readerTab = new ReaderEditTabForm(id);
    QTabWidget *mainTabs = this->findChild<QTabWidget*>("mainTabs");
    QString header = tr("Изменение читателя №");
    header += QString::number(id);
    int curIndex = mainTabs->addTab(readerTab,
                                    QIcon(QPixmap(":icons/user_change_info.png")),
                                    header);
    mainTabs->setCurrentIndex(curIndex);

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(readerTab, SIGNAL(readerUpdated(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_giveBookMenu_triggered()
{
    GiveBookForm *giveBookForm = new GiveBookForm;
    giveBookForm->show();

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(giveBookForm, SIGNAL(bookGived(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_returnBookMenu_triggered()
{
    ReturnBookForm *returnBookForm = new ReturnBookForm;
    returnBookForm->show();

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(returnBookForm, SIGNAL(bookReturned(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_exitMenu_triggered()
{
    if (confirmationMessage(QObject::tr("Вы действительно хотите выйти?")) == QMessageBox::Yes)
        qApp->exit();
}

void MainWindow::on_aboutQtMenu_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_removeBookMenu_triggered()
{
    RemoveBookForm *removeBookForm = new RemoveBookForm;
    removeBookForm->show();

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(removeBookForm, SIGNAL(bookRemoved(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_switchUserMenu_triggered()
{
    LoginForm *loginForm = new LoginForm;
    loginForm->setWindowModality(Qt::ApplicationModal);
    loginForm->show();
}

void MainWindow::on_configureMenu_triggered()
{
    SettingsForm *settingsForm = new SettingsForm;
    settingsForm->show();
}

void MainWindow::on_removeReaderMenu_triggered()
{
    RemoveReaderForm *form = new RemoveReaderForm;
    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(form, SIGNAL(readerRemoved(QString)),
                     statusBar, SLOT(showMessage(QString)));
    form->show();
}

void MainWindow::on_addBookMenu_triggered()
{
    AddBookForm *addBookForm = new AddBookForm;
    addBookForm->show();

    QStatusBar *statusBar = this->findChild<QStatusBar*>("statusBar");
    QObject::connect(addBookForm, SIGNAL(bookAdded(QString)),
                     statusBar, SLOT(showMessage(QString)));
}

void MainWindow::on_helpUserMenu_triggered()
{
    HelpBrowser *helpBrowser = new HelpBrowser(QString("user_index.html"));
    helpBrowser->showMaximized();
}

void MainWindow::on_aboutProgMenu_triggered()
{
    AboutForm *aboutForm = new AboutForm;
    aboutForm->show();
}

void MainWindow::on_helpAdminMenu_triggered()
{
    HelpBrowser *helpBrowser = new HelpBrowser(QString("admin_index.html"));
    helpBrowser->showMaximized();
}

void MainWindow::on_statsMenu_triggered()
{
    StatisticsForm *statForm = new StatisticsForm;
    statForm->show();
}
