#ifndef STATISTICS_FORM_H
#define STATISTICS_FORM_H

#include <QWidget>
#include <QTableView>
#include <QSqlTableModel>
#include <QPushButton>
#include <QLabel>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QHash>

#include "table_form.h"
#include "messages.h"
#include "ui_statistics_form.h"

namespace Ui {
class StatisticsForm;
}

class StatisticsForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit StatisticsForm(QWidget *parent = 0);
    ~StatisticsForm();
    
private slots:
    void on_okBtn_clicked();
    void on_showUnicBtn_clicked();
    void on_showGivenBtn_clicked();

    void on_showReadersBtn_clicked();

private:
    Ui::StatisticsForm *ui;
};

#endif // STATISTICS_FORM_H
