#include "get_book_id_form.h"

GetBookIdForm::GetBookIdForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GetBookIdForm)
{
    ui->setupUi(this);
}

GetBookIdForm::~GetBookIdForm()
{
    delete ui;
}

void GetBookIdForm::on_buttonBox_accepted()
{
    QSpinBox *idEdit = this->findChild<QSpinBox*>("idEdit");
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE id = :id");
    query.bindValue(":id", idEdit->value());
    if (!query.exec()) {
        errorMessage(QObject::tr("Не удалось выполнить запрос на существование книги "
                                 "c таким id.\n"
                                 "Код ошибки: 0112"));
        return;
    }
    query.next();
    if (!query.isValid()) {
        warningMessage(QObject::tr("В базе данных нет книги с таким id"));
        return;
    }
    emit editBookRequested(idEdit->value());
    this->close();
}

void GetBookIdForm::on_buttonBox_rejected()
{
    this->close();
}

void GetBookIdForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        if (pe->modifiers() & Qt::ControlModifier)
            on_buttonBox_accepted();
        break;
    case Qt::Key_Escape:
        on_buttonBox_rejected();
        break;
    default:
        break;
    }
}
