#include "statistics_form.h"

StatisticsForm::StatisticsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatisticsForm)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

    QSqlQuery query;
    query.prepare("SELECT COUNT(id) AS total FROM books");
    if (!query.exec()) {
        errorMessage(QObject::tr("Не удалось получить количество уникальных "
                                 "книг в библиотеке!"));
        qDebug() << query.lastError();
        return;
    }
    query.next();
    QLabel *unicCountLbl = this->findChild<QLabel*>("unicCountLbl");
    unicCountLbl->setText(unicCountLbl->text() + " " +
                          query.value(query.record().indexOf("total")).toString());

    QLabel *givenCountLbl = this->findChild<QLabel*>("givenCountLbl");
    if  (!query.exec("SELECT SUM(count) AS takCount FROM taken_books")) {
        errorMessage(QObject::tr("Не удалось получить количество "
                                 "взятых книг!"));
    }
    query.next();
    int takenCount = query.value(query.record().indexOf("takCount")).toInt();
    givenCountLbl->setText(givenCountLbl->text() + " " +
                           QString::number(takenCount));

    QLabel *allCountLbl = this->findChild<QLabel*>("allCountLbl");
    if (!query.exec("SELECT SUM(count) AS sumCount FROM books")) {
        errorMessage(QObject::tr("Не удалось получить общее количество книг "
                                 "в библиотеке!"));
        qDebug() << query.lastError();
        return;
    }
    query.next();
    int sumCount = query.value(query.record().indexOf("sumCount")).toInt();
    allCountLbl->setText(allCountLbl->text() + " " +
                         QString::number(sumCount + takenCount));


    QLabel *storeCountLbl = this->findChild<QLabel*>("storeCountLbl");
    storeCountLbl->setText(storeCountLbl->text() + " " +
                           QString::number(sumCount));

    QLabel *readersCountLbl = this->findChild<QLabel*>("readersCountLbl");
    if (!query.exec("SELECT COUNT(idReader) AS total FROM readers")) {
        errorMessage(QObject::tr("Не удалось получить количество зарегистрированных "
                                 "в библиотеке читателей!"));
        qDebug() << query.lastError();
        return;
    }
    query.next();
    readersCountLbl->setText(readersCountLbl->text() + " " +
                             query.value(query.record().indexOf("total")).toString());
}

StatisticsForm::~StatisticsForm()
{
    delete ui;
}

void StatisticsForm::on_okBtn_clicked()
{
    this->close();
}

void StatisticsForm::on_showUnicBtn_clicked()
{
    QString rowsCountQuery = "SELECT COUNT(id) AS total FROM books";
    QString getDataQuery = "SELECT id, name, isbn, udk, bbk, publisherName, editionNum, "
                           "YEAR(editionYear), count FROM books ORDER BY id LIMIT :lines";
    QString btnsQuery = "SELECT id, name, isbn, udk, bbk, publisherName, editionName, "
                        "YEAR(editionYear), count FROM books ORDER BY id LIMIT :pos, :lines";
    QString columnsName[] = {"id",
                             QObject::tr("Название книги"),
                             "ISBN",
                             QObject::tr("УДК"),
                             QObject::tr("ББК"),
                             QObject::tr("Издательство"),
                             QObject::tr("Номер издания"),
                             QObject::tr("Год издания"),
                             QObject::tr("В наличии")};
    int columnsWidth[] = {40,
                          400,
                          140,
                          140,
                          140,
                          140,
                          110,
                          90,
                          80};
    int countCol = 9;
//    QHash<QString, int> columnsMap;
    QString title = QObject::tr("Книги библиотеки");
    QString errorStr = QObject::tr("Не удалось получить список книг библиотеки!");
    int maxRows = 30;

    TableForm *bf = new TableForm (rowsCountQuery, getDataQuery, btnsQuery, columnsName,
                                   columnsWidth, countCol, title, errorStr, maxRows, UNICBOOKS);
    bf->showMaximized();
}

void StatisticsForm::on_showGivenBtn_clicked()
{
    QString rowsCountQuery = "SELECT COUNT(idBook) AS total FROM taken_books";
    QString getDataQuery = "SELECT readers.fstName, readers.secName, readers.patName, books.name, "
                           "taken_books.count FROM readers, books, taken_books "
                           "WHERE taken_books.idReader = readers.idReader "
                           "AND taken_books.idBook = books.id "
                           "ORDER BY taken_books.idReader LIMIT :lines";
    QString btnsQuery = "SELECT readers.fstName, readers.secName, readers.patName, books.name, "
            "taken_books.count FROM readers, books, taken_books "
            "WHERE taken_books.idReader = readers.idReader "
            "AND taken_books.idBook = books.id"
            "ORDER BY taken_books.idReader LIMIT :pos, :lines";
    QString columnsName[] = {QObject::tr("Фамилия"),
                             QObject::tr("Имя"),
                             QObject::tr("Отчество"),
                             QObject::tr("Название книги"),
                             QObject::tr("Количество")};
    int columnsWidth[] = {140,
                          140,
                          140,
                          400,
                          80};
    int countCol = 5;
//    QHash<QString, int> columnsMap;
    QString title = QObject::tr("Выданные книги");
    QString errorStr = QObject::tr("Не удалось получить список выданных книг!");
    int maxRows = 30;

    TableForm *bf = new TableForm (rowsCountQuery, getDataQuery, btnsQuery, columnsName,
                                   columnsWidth, countCol, title, errorStr, maxRows, GIVBOOKS);
    bf->showMaximized();
}

void StatisticsForm::on_showReadersBtn_clicked()
{
    QString rowsCountQuery = "SELECT COUNT(idReader) AS total FROM readers";
    QString getDataQuery = "SELECT idReader, fstName, secName, patName, passSer, passNum, other, "
                           "city, street, house, pav, room, homePhone, mobilePhone "
                           "FROM readers ORDER BY idReader LIMIT :lines";
    QString btnsQuery = "SELECT idReader, fstName, secName, patName, passSer, passNum, other, "
                        "city, street, house, pav, room, homePhone, mobilePhone "
                        "FROM readers ORDER BY idReader LIMIT :pos, :lines";
    QString columnsName[] = {"id",
                             QObject::tr("Фамилия"),
                             QObject::tr("Имя"),
                             QObject::tr("Отчество"),
                             QObject::tr("Серия паспорта"),
                             QObject::tr("Номер паспорта"),
                             QObject::tr("Другой документ"),
                             QObject::tr("Город"),
                             QObject::tr("Улица"),
                             QObject::tr("Дом"),
                             QObject::tr("Корпус"),
                             QObject::tr("Квартира"),
                             QObject::tr("Дом. телефон"),
                             QObject::tr("Моб. телефон")};
    int columnsWidth[] = {30,
                          100,
                          100,
                          100,
                          120,
                          120,
                          140,
                          160,
                          160,
                          35,
                          55,
                          70,
                          100,
                          100};
    int countCol = 14;

    QString title = QObject::tr("Читатели");
    QString errorStr = QObject::tr("Не удалось получить список читателей!");
    int maxRows = 30;

    TableForm *bf = new TableForm (rowsCountQuery, getDataQuery, btnsQuery, columnsName,
                                   columnsWidth, countCol, title, errorStr, maxRows, READERS);
    bf->showMaximized();
}
