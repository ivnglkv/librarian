#ifndef BOOK_H
#define BOOK_H

#include <QString>
#include <QDate>
#include <QtSql>
#include <QVector>

#include "messages.h"

class Book
{
public:
    Book();
    uint newBook();
    int addBook(uint);
    int removeBook(uint);
    int updateBook();

    uint getId();
    uint setId(const uint);
    QString getName();
    void setName(const QString &);
    QString getIsbn();
    void setIsbn(const QString &);
    QString getUdk();
    void setUdk(const QString &);
    QString getBbk();
    void setBbk(const QString &);
    QString getPublisherName();
    void setPublisherName(const QString &);
    int getEditionNum();
    void setEditionNum(const int);
    QDate getEditionYear();
    void setEditionYear(const QDate &);
    int getCount();
    void setCount(const int);
    bool exist(const QString &, const QString &isbn);

private:
    uint id;
    QString name;
    QString isbn;
    QString udk;
    QString bbk;
    QString publisherName;
    int editionNum;
    QDate editionYear;
    int count;
};

#endif // BOOK_H
