#include "interface_settings_form.h"

InterfaceSettingsForm::InterfaceSettingsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InterfaceSettingsForm)
{
    ui->setupUi(this);
}

InterfaceSettingsForm::~InterfaceSettingsForm()
{
    delete ui;
}

void InterfaceSettingsForm::on_styleBox_currentIndexChanged(const QString &style)
{
    QStyle *pStyle = QStyleFactory::create(style);
    QApplication::setStyle(pStyle);
}

void InterfaceSettingsForm::saveSettings()
{
    QComboBox *styleBox = this->findChild<QComboBox*>("styleBox");
    QSettings *settings = Librarian::librarianApp()->settings();
    settings->setValue("/Interface/Style", styleBox->currentText());
}

void InterfaceSettingsForm::rejectSettings()
{
    QSettings *settings = Librarian::librarianApp()->settings();
    QStyle *pStyle = QStyleFactory::create(settings->value("/Interface/Style", "Gtk").toString());
    QApplication::setStyle(pStyle);
}
