#include "librarian.h"
#include "mainwindow.h"
#include "login_form.h"

int main(int argc, char *argv[])
{
    Librarian librarian(argc, argv, "PKGH", "Librarian");
    MainWindow mainWin;

    mainWin.showMaximized();

    LoginForm *loginForm = new LoginForm;
    loginForm->show();

    return librarian.exec();
}
