#ifndef READERTAB_H
#define READERTAB_H

#include <QtGui>
#include <QtUiTools>

#include "ui_reader_tab.h"
#include "reader.h"
#include "messages.h"
#include "convert_date.h"
#include "validators.h"

namespace Ui {
class ReaderTabForm;
}

class ReaderTabForm : public QWidget
{
    Q_OBJECT
public:
    explicit ReaderTabForm(QWidget *parent = 0);
    ~ReaderTabForm();

private slots:
    void on_okButton_clicked();
    void on_otherBox_currentIndexChanged(int index);

private:
    Ui::ReaderTabForm *ui;
    Reader *reader;

signals:
    void readerAdded(QString);
};

#endif // READERTAB_H
