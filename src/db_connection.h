#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QtSql>
#include <QSettings>

#include "librarian.h"

bool createConnection(QString &, QString &);

#endif // DBCONNECTION_H
