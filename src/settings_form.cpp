#include "settings_form.h"

SettingsForm::SettingsForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsForm)
{
    ui->setupUi(this);

    QListWidget *groupsList = this->findChild<QListWidget*>("groupsList");
    QStringList lst;
    QListWidgetItem *pItem = 0;
    lst << "interface" << "user_config" << "network" << "organization";
    QStringList names;
    names << QObject::tr("Интерфейс") << QObject::tr("Пользователи") << QObject::tr("Сеть")
          << QObject::tr("Организация");
    for (int i = 0; (i < lst.size()) && (i < names.size()); i++) {
        pItem = new QListWidgetItem(names.at(i), groupsList);
        pItem->setIcon(QPixmap(":icons/" + lst.at(i) + ".png"));
    }
    QStackedWidget *settingsStack = this->findChild<QStackedWidget*>("settingsStack");

    // Creating widgets for settingsStack
    InterfaceSettingsForm *interfaceSettings = new InterfaceSettingsForm;
    UsersSettingsForm *usersSettings = new UsersSettingsForm;
    NetworkSettingsForm *networkSettings = new NetworkSettingsForm;
    OrganizationSettingsForm *orgSettings = new OrganizationSettingsForm;

    // Connecting signal saveSettings (emitted on buttonBox_accepted)
    // with slots in widgets from settingsStack, saying 'em to save current settings
    QObject::connect(this, SIGNAL(saveSettings()),
                     interfaceSettings, SLOT(saveSettings()));
    QObject::connect(this, SIGNAL(saveSettings()),
                     usersSettings, SLOT(saveSettings()));
    QObject::connect(this, SIGNAL(saveSettings()),
                     networkSettings, SLOT(saveSettings()));
    QObject::connect(this, SIGNAL(saveSettings()),
                     orgSettings, SLOT(saveSettings()));

    // Connecting signal rejectSettings (emitted on buttonBox_rejected)
    // with slots in widgets from settingsStack, saying 'em not to save current settings
    // Now only InterfaceSettingsForm is applying settings immediately
    QObject::connect(this, SIGNAL(rejectSettings()),
                     interfaceSettings, SLOT(rejectSettings()));

    settingsStack->addWidget(interfaceSettings);
    settingsStack->addWidget(usersSettings);
    settingsStack->addWidget(networkSettings);
    settingsStack->addWidget(orgSettings);

    this->setMinimumWidth(750);
}

SettingsForm::~SettingsForm()
{
    delete ui;
}

void SettingsForm::on_groupsList_itemClicked(QListWidgetItem *item)
{
    QStackedWidget *settingsStack = this->findChild<QStackedWidget*>("settingsStack");
    if (item->text() == QObject::tr("Интерфейс")) {
        settingsStack->setCurrentIndex(0);
    } else if (item->text() == QObject::tr("Пользователи")) {
        settingsStack->setCurrentIndex(1);
    } else if (item->text() == QObject::tr("Сеть")) {
        settingsStack->setCurrentIndex(2);
    } else if (item->text() == QObject::tr("Организация")) {
        settingsStack->setCurrentIndex(3);
    }
}

void SettingsForm::on_buttonBox_accepted()
{
    emit saveSettings();
    this->close();
}

void SettingsForm::on_buttonBox_rejected()
{
    emit rejectSettings();
    this->close();
}
