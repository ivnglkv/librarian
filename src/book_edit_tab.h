#ifndef BOOK_EDIT_TAB_H
#define BOOK_EDIT_TAB_H

#include <QtGui>
#include <QtUiTools>
#include <QtSql>

#include "ui_book_edit_tab.h"
#include "book.h"
#include "author.h"
#include "validators.h"
#include "messages.h"

namespace Ui {
class BookEditTabForm;
}

class BookEditTabForm : public QWidget
{
    Q_OBJECT
public:
    explicit BookEditTabForm(int id, QString &authorsStr, QWidget *parent = 0);
    explicit BookEditTabForm(int id, QWidget *parent = 0);
    ~BookEditTabForm();
    
private slots:
    void on_okButton_clicked();

private:
    Ui::BookEditTabForm *ui;
    Book *book;
    Author *author;
    QVector<uint> oldAuthorsIdList;
    QVector<uint> newAuthorsIdList;

signals:
    void bookUpdated(QString);
};

#endif // BOOK_EDIT_TAB_H
