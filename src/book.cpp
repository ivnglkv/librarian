#include "book.h"

Book::Book(): isbn(""),
    udk(""),
    bbk(""),
    publisherName("")
{
}

// This function insert information about new book in database.
// If query fails then 1 is returned.
// Else 0 is returned.
uint Book::newBook()
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QSqlQuery query;
    query.prepare("INSERT INTO books "
                  "VALUES (NULL, :name, :isbn, :udk, :bbk, "
                  ":publisherName, :editionNum, :editionYear, :count);");
    query.bindValue(":name", name);
    if (isbn.isEmpty()) {
        query.bindValue(":isbn", QVariant(QVariant::String));
    } else {
        query.bindValue(":isbn", isbn);
    }
    if (udk.isEmpty()) {
        query.bindValue(":udk", QVariant(QVariant::String));
    } else {
        query.bindValue(":udk", udk);
    }
    if (bbk.isEmpty()) {
        query.bindValue(":bbk", QVariant(QVariant::String));
    } else {
        query.bindValue(":bbk", bbk);
    }
    query.bindValue(":publisherName", publisherName);
    query.bindValue(":editionNum", editionNum);
    query.bindValue(":editionYear", editionYear);
    query.bindValue(":count", count);

    if (!query.exec()) {
        qDebug() << "Unable to make insert book operation";
        errorMessage(QObject::tr("Не удалось вставить запись в базу данных.\n"
                        "Попробуйте еще раз или обратитесь к администратору.\n"
                        "Код ошибки: 0002"));
        qDebug() << query.lastError();
        return 0;
    }

    return query.lastInsertId().toUInt();
}

int Book::updateBook()
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QSqlQuery query;
    query.prepare("REPLACE INTO books "
                  "VALUES (:id, :name, :isbn, :udk, :bbk, "
                  ":publisherName, :editionNum, :editionYear, :count);");
    query.bindValue(":id", id);
    query.bindValue(":name", name);
    if (isbn.isEmpty()) {
        query.bindValue(":isbn", QVariant(QVariant::String));
    } else {
        query.bindValue(":isbn", isbn);
    }
    if (udk.isEmpty()) {
        query.bindValue(":udk", QVariant(QVariant::String));
    } else {
        query.bindValue(":udk", udk);
    }
    if (bbk.isEmpty()) {
        query.bindValue(":bbk", QVariant(QVariant::String));
    } else {
        query.bindValue(":bbk", bbk);
    }
    query.bindValue(":publisherName", publisherName);
    query.bindValue(":editionNum", editionNum);
    query.bindValue(":editionYear", editionYear);
    query.bindValue(":count", count);

    if (!query.exec()) {
        qDebug() << "Unable to make insert book operation";
        errorMessage(QObject::tr("Не удалось вставить запись в базу данных.\n"
                        "Попробуйте еще раз или обратитесь к администратору.\n"
                        "Код ошибки: 0401"));
        qDebug() << query.lastError();
        return 0;
    }

    return 1;
}

int Book::addBook(uint count)
{
    QSqlQuery selectCurrentCount;
    selectCurrentCount.prepare("SELECT count FROM books WHERE id = :id");
    selectCurrentCount.bindValue(":id", id);
    if (!selectCurrentCount.exec()) {
        qDebug() << "Error: Can't retrive" << id << "books count";
        qDebug() << selectCurrentCount.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос на получение текущего "
                                 "количества книг в хранилище.\n"
                                 "Код ошибки: 0101"));
        return 0;
    }
    selectCurrentCount.next();
    if (!selectCurrentCount.isValid()) {
        qDebug() << "Warning: book with id" << id << "doesn't exist";
        errorMessage(QObject::tr("Книги с таким id не существует.\n"
                                 "Код ошибки: 0201"));
        return 0;
    }

    QSqlRecord currentCountRecord = selectCurrentCount.record();
    int currentCountCol = currentCountRecord.indexOf("count");

    QSqlQuery insertNewValue;
    insertNewValue.prepare("UPDATE books SET count = :count WHERE id = :id");
    insertNewValue.bindValue(":count", selectCurrentCount.value(currentCountCol).toUInt() + count);
    insertNewValue.bindValue(":id", id);
    if (!insertNewValue.exec()) {
        qDebug() << "Error: can't add books to storage";
        qDebug() << insertNewValue.lastError();
        errorMessage(QObject::tr("Не удалось добавить книги.\n"
                                 "Код ошибки: 0301"));
        return 0;
    }

    return 1;
}

int Book::removeBook(uint count)
{
    QSqlQuery selectCurrentCount;
    selectCurrentCount.prepare("SELECT count FROM books WHERE id = :id");
    selectCurrentCount.bindValue(":id", id);
    if (!selectCurrentCount.exec()) {
        qDebug() << "Error: Can't retrive" << id << "books count";
        qDebug() << selectCurrentCount.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос на получение текущего "
                                 "количества книг в хранилище.\n"
                                 "Код ошибки: 0102"));
        return 0;
    }
    selectCurrentCount.next();
    if (!selectCurrentCount.isValid()) {
        qDebug() << "Warning: book with id" << id << "doesn't exist";
        errorMessage(QObject::tr("Книги с таким id не существует.\n"
                                 "Код ошибки: 0202"));
        return 0;
    }

    QSqlRecord currentCountRecord = selectCurrentCount.record();
    int currentCountCol = currentCountRecord.indexOf("count");

    uint currentCount = selectCurrentCount.value(currentCountCol).toUInt();
    if (currentCount < count) {
        qDebug() << "Warning: trying to remove" << count << "books, but there are only"
                 << currentCount << "books in storage";
        QString message = QObject::tr("Сейчас в библиотеке находится только ");
        message += QString::number(currentCount);
        switch (currentCount % 10) {
        case 1:
            if (currentCount != 11) {
                message += QObject::tr(" книга");
                break;
            }
        case 2:
        case 3:
        case 4:
            if ((currentCount != 11)
                    && (currentCount != 12)
                    && (currentCount != 13)
                    && (currentCount != 14)) {
                message += QObject::tr(" книги");
                break;
            }
        default:
            message += QObject::tr(" книг");
        }
        warningMessage(message);
        return 0;
    }

    QSqlQuery insertNewValue;
    insertNewValue.prepare("UPDATE books SET count = :count WHERE id = :id");
    insertNewValue.bindValue(":count", selectCurrentCount.value(currentCountCol).toUInt() - count);
    insertNewValue.bindValue(":id", id);
    if (!insertNewValue.exec()) {
        qDebug() << "Error: can't remove books from storage";
        qDebug() << insertNewValue.lastError();
        errorMessage(QObject::tr("Не удалось списать книги.\n"
                                 "Код ошибки: 0302"));
        return 0;
    }

    return 1;
}

uint Book::getId()
{
    return id;
}

uint Book::setId(const uint newId)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE id = :id");
    query.bindValue(":id", newId);
    if (!query.exec()) {
        qDebug() << "Error: can't select book" << newId;
        qDebug() << query.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос на поиск книги в базе данных.\n"
                                 "Код ошибки: 0103"));
        return 0;
    }

    query.next();
    if (!query.isValid()) {
        qDebug() << "Warning: book" << newId<< "wasn't found";
        warningMessage(QObject::tr("В базе данных нет книги с таким id"));
        return 0;
    }

    id = newId;
    return id;
}

QString Book::getName()
{
    if (name == "") {
        QSqlQuery query;
        query.prepare("SELECT name FROM books WHERE id = :id");
        query.bindValue(":id", id);
        if (!query.exec()) {
            qDebug() << "Error: can't select book" << id;
            qDebug() << query.lastError();
            return "";
        }
        query.next();
        if (!query.isValid()) {
            qDebug() << "Warning: book" << id << "wasn't found";
            return "";
        }
        QSqlRecord idRecord = query.record();
        int bookNameCol = idRecord.indexOf("name");
        return query.value(bookNameCol).toString();
    }
    return name;
}

void Book::setName(const QString &newName)
{
    name = newName;
}

QString Book::getIsbn()
{
    return isbn;
}

void Book::setIsbn(const QString &newIsbn)
{
    isbn = newIsbn;
}

QString Book::getUdk()
{
    return udk;
}

void Book::setUdk(const QString &newUdk)
{
    udk = newUdk;
}

QString Book::getBbk()
{
    return bbk;
}

void Book::setBbk(const QString &newBbk)
{
    bbk = newBbk;
}

QString Book::getPublisherName()
{
    return publisherName;
}

void Book::setPublisherName(const QString &newPublisherName)
{
    publisherName = newPublisherName;
}

int Book::getEditionNum()
{
    return editionNum;
}

void Book::setEditionNum(const int newEditionNum)
{
    editionNum = newEditionNum;
}

QDate Book::getEditionYear()
{
    return editionYear;
}

void Book::setEditionYear(const QDate &newEditionYear)
{
    editionYear = newEditionYear;
}

int Book::getCount()
{
    return count;
}

void Book::setCount(const int newCount)
{
    count = newCount;
}

bool Book::exist(const QString &name, const QString &isbn)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE name = :name AND isbn = :isbn");
    query.bindValue(":name", name);
    query.bindValue(":isbn", isbn);
    if (!query.exec()) {
        qDebug() << "Error: unable to execute query";
        qDebug() << query.lastError();
        errorMessage(QObject::tr("Не удалось выполнить запрос к базе данных.\n"
                                 "Код ошибки: 0104"));
        return true;
    }
    query.next();
    if (!query.isValid()) {
        qDebug() << "Info: no such book in database";
        return false;
    } else {
        qDebug() << "Info: book already exists in database";
        return true;
    }
}
