#ifndef ORGANIZATION_SETTINGS_FORM_H
#define ORGANIZATION_SETTINGS_FORM_H

#include <QWidget>
#include <QSettings>
#include <QLineEdit>

#include "librarian.h"
#include "ui_organization_settings_form.h"

namespace Ui {
class OrganizationSettingsForm;
}

class OrganizationSettingsForm : public QWidget
{
    Q_OBJECT

public:
    explicit OrganizationSettingsForm(QWidget *parent = 0);
    ~OrganizationSettingsForm();

public slots:
    void saveSettings();

private:
    Ui::OrganizationSettingsForm *ui;
};

#endif // ORGANIZATION_SETTINGS_FORM_H
