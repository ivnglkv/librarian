#ifndef GIVE_BOOK_FORM_H
#define GIVE_BOOK_FORM_H

#include <QWidget>

#include "reader.h"
#include "book.h"
#include "ui_give_book_form.h"

namespace Ui {
class GiveBookForm;
}

class GiveBookForm : public QWidget
{
    Q_OBJECT
    
public:
    explicit GiveBookForm(QWidget *parent = 0);
    ~GiveBookForm();
    
private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::GiveBookForm *ui;
    Reader *reader;
    Book *book;

    void keyPressEvent(QKeyEvent *);

signals:
    void bookGived(QString);
};

#endif // GIVE_BOOK_FORM_H
