#include "search_book_tab.h"

QVector<int> booksIdList;

SearchBookTabForm::SearchBookTabForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SearchBookTabForm)
// Create widget for placing into QTabWidget. This method executes one query to db in order
// to find author names, which are placed into authorEdit QComboBox
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

    BookProfileForm *bookProfile = new BookProfileForm;
    QHBoxLayout *searchLayout = this->findChild<QHBoxLayout*>("horizontalLayout_7");
    searchLayout->addWidget(bookProfile, 3);

    QObject::connect(this, SIGNAL(showBookProfile(int)),
                     bookProfile, SLOT(showProfile(int)));
    QObject::connect(bookProfile, SIGNAL(editBook(int, QString &)),
                     this, SLOT(on_editBookRequested(int, QString &)));
}

SearchBookTabForm::~SearchBookTabForm()
{
    delete ui;
}

void SearchBookTabForm::on_pushButton_clicked()
{
    QSpinBox *idEdit = this->findChild<QSpinBox*>("idEdit");
    QLineEdit *nameEdit = this->findChild<QLineEdit*>("nameEdit");
    QLineEdit *authorNameEdit = this->findChild<QLineEdit*>("authorNameEdit");
    QComboBox *idenBox= this->findChild<QComboBox*>("idenBox");
    QLineEdit *idenEdit = this->findChild<QLineEdit*>("idenEdit");

    // If search conditions aren't set, then show error message and quit from function
    if ((idEdit->value() == 0) &&
            (nameEdit->text().isEmpty()) &&
            (authorNameEdit->text().isEmpty()) &&
            (idenBox->currentIndex() == 0)) {
        qDebug() << "Error: trying to search without any conditions";
        errorMessage(QObject::tr("Необходимо задать хотя бы одно условие поиска.\n"
                                 "Код ошибки: 1039"));
        return;
    }

    QSqlQuery selectBooks;
    QString queryString = "SELECT id, name FROM books WHERE ";
    bool hasPrevious = false;

//    Generating string to execute in query
    if (idEdit->value() != 0) {
        queryString += "id = ";
        queryString += idEdit->text();
        hasPrevious = true;
    }
    if (!nameEdit->text().isEmpty()) {
        if (hasPrevious) {
            queryString += " AND ";
        } else {
            hasPrevious = true;
        }
        queryString += "name LIKE '%";
        queryString += nameEdit->text();
        queryString += "%' ";
    }
    if (!authorNameEdit->text().isEmpty()) {
        if (hasPrevious) {
            queryString += " AND ";
        } else {
            hasPrevious = true;
        }

        queryString += "id IN (SELECT idBook FROM books_authors WHERE "
                       "idAuthor IN (SELECT id FROM authors WHERE name LIKE '%";

        QStringList authors = authorNameEdit->text().split(", ");
        uint authorsCount = authors.size();

        for (uint i = 0; i < authorsCount; i++) {
            queryString += authors[i];
            queryString += "%' ";
            if (i < (authorsCount - 1))
                queryString += "OR name LIKE '%";
        }
        queryString += "))";
    }
    if (idenBox->currentIndex() > 0) {
//        If any identificator is choosen but no text were entered then error is generated
        if (idenEdit->text().isEmpty()) {
            errorMessage(QObject::tr("Введите значение идентификатора.\n"
                                     "Код ошибки: 1040"));
            return;
        }
        if (hasPrevious) {
            queryString += " AND ";
        } else {
            hasPrevious = true;
        }
        switch (idenBox->currentIndex()) {
        case 1:
            queryString += "isbn = '";
            break;
        case 2:
            queryString += "udk = ";
            break;
        case 3:
            queryString += "bbk = ";
            break;
        default:
            return;
            break;
        }
        queryString += idenEdit->text();
        queryString += "'";
    }
    queryString += ";";
//    End of queryString generating

    if (!selectBooks.exec(queryString)) {
        qDebug() << "Error occured while trying to execute select operation";
        errorMessage(QObject::tr("Не удалось произвести поиск.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0122"));
        return;
    }

//    Forming list of found books
    QSqlRecord book;
    book = selectBooks.record();

    int bookIdCol = book.indexOf("id");
    int bookNameCol = book.indexOf("name");

    QStringList booksNamesList;
    booksIdList.clear();

    while (selectBooks.next()) {
        booksIdList << selectBooks.value(bookIdCol).toInt();
        booksNamesList << selectBooks.value(bookNameCol).toString();
    }
//    End of list forming

//    Filling the listView with found values
    QStringListModel *booksNamesModel = new QStringListModel;
    booksNamesModel->setStringList(booksNamesList);

    QListView *listView = this->findChild<QListView*>("listView");

    listView->setModel(booksNamesModel);

//    Forming message to be shown in status bar
    QString message = QObject::tr("Найдено ");
    message += QString::number(booksIdList.size());

    switch (booksIdList.size() % 10) {
    case 1:
        if (booksIdList.size() != 11) {
            message += QObject::tr(" совпадение");
            break;
        }
    case 2:
    case 3:
    case 4:
        if ((booksIdList.size() != 11)
                && (booksIdList.size() != 12)
                && (booksIdList.size() != 13)
                && (booksIdList.size() != 14)) {
            message += QObject::tr(" совпадения");
            break;
        }
    default:
        message += QObject::tr(" совпадений");
        break;
    }

    emit booksFound(message);
}

void SearchBookTabForm::on_idenBox_currentIndexChanged(int index)
{
    QLineEdit *idenEdit = this->findChild<QLineEdit*>("idenEdit");
    if (index > 0)
        idenEdit->setEnabled(true);
    else
        idenEdit->setEnabled(false);
}

void SearchBookTabForm::on_listView_clicked(const QModelIndex &index)
{
    int id = booksIdList[index.row()];
    emit showBookProfile(id);
}

void SearchBookTabForm::on_editBookRequested(int id, QString &authors)
{
    emit editBookRedirect(id, authors);
}
