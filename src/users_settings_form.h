#ifndef USERS_SETTINGS_FORM_H
#define USERS_SETTINGS_FORM_H

#include "ui_users_settings_form.h"

namespace Ui {
class UserSettingsForm;
}

class UsersSettingsForm : public QWidget
{
    Q_OBJECT
public:
    explicit UsersSettingsForm(QWidget *parent = 0);
    ~UsersSettingsForm();

public slots:
    void saveSettings();

private:
    Ui::UserSettingsForm *ui;
};

#endif // USERS_SETTINGS_FORM_H
