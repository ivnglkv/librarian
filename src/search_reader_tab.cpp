#include "search_reader_tab.h"

QVector<int>readersIdList;

SearchReaderTabForm::SearchReaderTabForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SearchReaderTabForm)
{
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    ui->setupUi(this);

    ReaderProfileForm *readerProfile = new ReaderProfileForm;
    QHBoxLayout *searchLayout = this->findChild<QHBoxLayout*>("horizontalLayout_6");
    searchLayout->addWidget(readerProfile, 3);

    QObject::connect(this, SIGNAL(showReaderProfile(int)),
                     readerProfile, SLOT(showProfile(int)));
    QObject::connect(readerProfile, SIGNAL(editReader(int)),
                     this, SLOT(editBookRedirect(int)));
}

SearchReaderTabForm::~SearchReaderTabForm()
{
    delete ui;
}

void SearchReaderTabForm::on_searchButton_clicked()
{
    QSpinBox *readerIdEdit = this->findChild<QSpinBox*>("readerIdEdit");
    QLineEdit *readerFstNameEdit = this->findChild<QLineEdit*>("readerFstNameEdit");
    QLineEdit *readerSecNameEdit = this->findChild<QLineEdit*>("readerSecNameEdit");
    QLineEdit *readerPatNameEdit = this->findChild<QLineEdit*>("readerPatNameEdit");

    if (readerIdEdit->value() == 0
            && readerFstNameEdit->text().isEmpty()
            && readerSecNameEdit->text().isEmpty()
            && readerPatNameEdit->text().isEmpty()) {
        qDebug() << "Error: trying to search without any conditions";
        errorMessage(QObject::tr("Необходимо задать хотя бы одно условие поиска.\n"
                                 "Код ошибки: 1041"));
        return;
    }

    QString queryString = "SELECT idReader, fstName, secName, patName FROM readers WHERE ";
    bool hasPrevious = false;

//    Forming string for search query
    if (readerIdEdit->value() > 0) {
        queryString += "idReader = ";
        queryString += readerIdEdit->text();
        hasPrevious = true;
    }

    if (!readerFstNameEdit->text().isEmpty()) {
        if (hasPrevious) {
            queryString += " AND ";
        } else {
            hasPrevious = true;
        }
        queryString += "fstName LIKE '%";
        queryString += readerFstNameEdit->text();
        queryString += "%'";
    }

    if (!readerSecNameEdit->text().isEmpty()) {
        if (hasPrevious) {
            queryString += " AND ";
        } else {
            hasPrevious = true;
        }
        queryString += "secName LIKE '%";
        queryString += readerSecNameEdit->text();
        queryString += "%'";
    }

    if (!readerPatNameEdit->text().isEmpty()) {
        if (hasPrevious) {
            queryString += " AND ";
        } else {
            hasPrevious = true;
        }
        queryString += "patName LIKE '%";
        queryString += readerPatNameEdit->text();
        queryString += "%'";
    }

    queryString += ";";
//    End of query string generating

    QSqlQuery selectReaders;

    if (!selectReaders.exec(queryString)) {
        qDebug() << "Error occured while trying to execute select operation";
        errorMessage(QObject::tr("Не удалось произвести поиск.\n"
                                 "Попробуйте еще раз или обратитесь к администратору.\n"
                                 "Код ошибки: 0123"));
        return;
    }

    QSqlRecord reader;
    reader = selectReaders.record();

    int idCol = reader.indexOf("idReader");
    int fstNameCol = reader.indexOf("fstName");
    int secNameCol = reader.indexOf("secName");
    int patNameCol = reader.indexOf("patName");

    QStringList readersNamesList;
    readersIdList.clear();

    while (selectReaders.next()) {
        readersIdList << selectReaders.value(idCol).toInt();
        QString tmpName = "";
        tmpName += selectReaders.value(fstNameCol).toString();
        tmpName += " ";
        tmpName += selectReaders.value(secNameCol).toString();
        tmpName += " ";
        tmpName += selectReaders.value(patNameCol).toString();
        readersNamesList << tmpName;
    }

//    Filling the listView with found values
    QStringListModel *readersNamesModel = new QStringListModel;
    readersNamesModel->setStringList(readersNamesList);

    QListView *listView = this->findChild<QListView*>("shortResultsView");

    listView->setModel(readersNamesModel);

//    Forming message to be shown in status bar
    QString message = QObject::tr("Найдено ");
    message += QString::number(readersIdList.size());

    switch (readersIdList.size() % 10) {
    case 1:
        if (readersIdList.size() != 11) {
            message += QObject::tr(" совпадение");
            break;
        }
    case 2:
    case 3:
    case 4:
        if ((readersIdList.size() != 11)
                && (readersIdList.size() != 12)
                && (readersIdList.size() != 13)
                && (readersIdList.size() != 14)) {
            message += QObject::tr(" совпадения");
            break;
        }
    default:
        message += QObject::tr(" совпадений");
        break;
    }

    emit readersFound(message);
}

void SearchReaderTabForm::on_shortResultsView_clicked(const QModelIndex &index)
{
    int id = readersIdList[index.row()];
    emit showReaderProfile(id);
}

void SearchReaderTabForm::editBookRedirect(int id)
{
    emit editBook(id);
}
