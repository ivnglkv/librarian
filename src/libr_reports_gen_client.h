#ifndef LIBR_REPORTS_GEN_CLIENT_H
#define LIBR_REPORTS_GEN_CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include "report_types.h"

class LibrReportsGenClient : public QObject
{
    Q_OBJECT
public:
    explicit LibrReportsGenClient(const QString& strHost, int nPort, QObject *parent = 0);
    void slotSendToServer(ReportTypes type, uint id);

private:
    QTcpSocket* tcpSocket;
    quint16 nextBlockSize;
private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);

signals:
    void gotAnswer(QString);
};

#endif // LIBR_REPORTS_GEN_CLIENT_H
