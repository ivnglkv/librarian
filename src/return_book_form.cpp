#include "return_book_form.h"

ReturnBookForm::ReturnBookForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReturnBookForm),
    reader(new Reader),
    book(new Book)
{
    ui->setupUi(this);
}

ReturnBookForm::~ReturnBookForm()
{
    delete ui;
    delete reader;
    delete book;
}

void ReturnBookForm::on_buttonBox_accepted()
{
    QSpinBox *idReaderBox = this->findChild<QSpinBox*>("idReaderBox");
    if (!reader->setIdReader(idReaderBox->value()))
        return;

    QSpinBox *idBookBox = this->findChild<QSpinBox*>("idBookBox");
    if (!book->setId(idBookBox->value()))
        return;

    QSpinBox *countBox = this->findChild<QSpinBox*>("countBox");
    uint count = countBox->value();

    QString confirmString = QObject::tr("Возвратить ");
    confirmString += QString::number(count);
    switch (count % 10) {
    case 1:
        if (count != 11) {
            confirmString += QObject::tr(" книгу №");
            break;
        }
    case 2:
    case 3:
    case 4:
        if ((count != 11)
                && (count != 12)
                && (count != 13)
                && (count != 14)) {
            confirmString += QObject::tr(" книги №");
            break;
        }
    default:
        confirmString += QObject::tr(" книг №");
        break;
    }
    confirmString += QString::number(book->getId())
            + " ("
            + book->getName()
            + QObject::tr(") от читателя №")
            + QString::number(reader->getIdReader())
            + " ("
            + reader->getFstName() + " "
            + reader->getSecName() + " "
            + reader->getPatName()
            + ")?";
    if (confirmationMessage(confirmString) == QMessageBox::No)
        return;

    if (reader->returnBook(book->getId(), count)) {
        QString message = QObject::tr("Читатель №");
        message += QString::number(reader->getIdReader());
        message += QObject::tr(" возвратил ");
        message += QString::number(count);

        switch (count % 10) {
        case 1:
            if (count != 11) {
                message += QObject::tr(" книгу №");
                break;
            }
        default:
            if (((count % 10) != 0)
                    && (count != 11)
                    && (count != 12)
                    && (count != 13)
                    && (count != 14)) {
                message += QObject::tr(" книги №");
            } else {
                message += QObject::tr(" книг №");
            }
            break;
        }

        message += QString::number(book->getId());
        emit bookReturned(message);
    }
}

void ReturnBookForm::on_buttonBox_rejected()
{
    this->close();
}

void ReturnBookForm::keyPressEvent(QKeyEvent *pe)
{
    switch (pe->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        if (pe->modifiers() & Qt::ControlModifier)
            on_buttonBox_accepted();
        break;
    case Qt::Key_Escape:
        on_buttonBox_rejected();
        break;
    default:
        break;
    }
}
