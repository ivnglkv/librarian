#ifndef ERRORS_H
#define ERRORS_H

#include <QString>
#include <QtGui>
// Show error message with text given in parameter
void errorMessage (const QString &);

// Show warning message with text given in parameter
void warningMessage (const QString &);

int confirmationMessage (const QString &);

#endif // ERRORS_H
