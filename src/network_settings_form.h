#ifndef NETWORK_SETTINGS_FORM_H
#define NETWORK_SETTINGS_FORM_H

#include "librarian.h"
#include "login_form.h"
#include "db_connection.h"
#include "ui_network_settings_form.h"

namespace Ui {
class NetworkSettingsForm;
}

class NetworkSettingsForm : public QWidget
{
    Q_OBJECT
public:
    explicit NetworkSettingsForm(QWidget *parent = 0);
    ~NetworkSettingsForm();

public slots:
    void saveSettings();

private:
    Ui::NetworkSettingsForm *ui;
};

#endif // NETWORK_SETTINGS_FORM_H
