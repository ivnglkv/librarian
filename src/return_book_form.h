#ifndef RETURN_BOOK_FORM_H
#define RETURN_BOOK_FORM_H

#include <QWidget>

#include "reader.h"
#include "book.h"
#include "ui_return_book_form.h"

namespace Ui {
class ReturnBookForm;
}

class ReturnBookForm : public QWidget
{
    Q_OBJECT
public:
    explicit ReturnBookForm(QWidget *parent = 0);
    ~ReturnBookForm();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::ReturnBookForm *ui;
    Reader *reader;
    Book *book;

    void keyPressEvent(QKeyEvent *);

signals:
    void bookReturned(QString);
};

#endif // RETURN_BOOK_FORM_H
