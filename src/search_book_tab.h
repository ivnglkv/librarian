#ifndef SEARCH_BOOK_TAB_H
#define SEARCH_BOOK_TAB_H

#include <QtGui>
#include <QtUiTools>
#include <QtSql>

#include "messages.h"
#include "ui_search_book_tab.h"
#include "book_profile.h"

namespace Ui {
class SearchBookTabForm;
}

class SearchBookTabForm : public QWidget
{
    Q_OBJECT
public:
    explicit SearchBookTabForm(QWidget *parent = 0);
    ~SearchBookTabForm();

private slots:
    void on_pushButton_clicked();
    void on_idenBox_currentIndexChanged(int index);
    void on_listView_clicked(const QModelIndex &index);
    void on_editBookRequested(int id, QString &authors);

signals:
    void showBookProfile(int id);
    void editBookRedirect(int id, QString &authors);
    void booksFound(QString);

private:
    Ui::SearchBookTabForm *ui;
};

#endif // SEARCH_BOOK_TAB_H
