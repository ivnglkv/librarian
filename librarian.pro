#-------------------------------------------------
#
# Project created by QtCreator 2012-10-04T16:25:13
#
#-------------------------------------------------

QT       += core gui sql webkit network

TARGET = librarian
TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/book.cpp \
    src/reader.cpp \
    src/book_tab.cpp \
    src/reader_tab.cpp \
    src/search_reader_tab.cpp \
    src/search_book_tab.cpp \
    src/reader_profile.cpp \
    src/book_profile.cpp \
    src/book_edit_tab.cpp \
    src/author.cpp \
    src/reader_edit_tab.cpp \
    src/give_book_form.cpp \
    src/return_book_form.cpp \
    src/remove_book_form.cpp \
    src/messages.cpp \
    src/librarian.cpp \
    src/login_form.cpp \
    src/settings_form.cpp \
    src/network_settings_form.cpp \
    src/interface_settings_form.cpp \
    src/users_settings_form.cpp \
    src/get_book_id_form.cpp \
    src/get_reader_id_form.cpp \
    src/remove_reader_form.cpp \
    src/add_book_form.cpp \
    src/help_browser.cpp \
    src/about_form.cpp \
    src/db_connection.cpp \
    src/statistics_form.cpp \
    src/table_form.cpp \
    src/libr_print.cpp \
    src/organization_settings_form.cpp \
    src/libr_reports_gen_client.cpp

HEADERS  += src/mainwindow.h \
    src/book.h \
    src/reader.h \
    src/validators.h \
    src/search_reader_tab.h \
    src/reader_tab.h \
    src/db_connection.h \
    src/book_tab.h \
    src/search_book_tab.h \
    src/reader_profile.h \
    src/book_profile.h \
    src/book_edit_tab.h \
    src/author.h \
    src/reader_edit_tab.h \
    src/give_book_form.h \
    src/return_book_form.h \
    src/remove_book_form.h \
    src/messages.h \
    src/librarian.h \
    src/login_form.h \
    src/settings_form.h \
    src/network_settings_form.h \
    src/interface_settings_form.h \
    src/users_settings_form.h \
    src/get_book_id_form.h \
    src/get_reader_id_form.h \
    src/remove_reader_form.h \
    src/add_book_form.h \
    src/help_browser.h \
    src/about_form.h \
    src/statistics_form.h \
    src/table_form.h \
    src/libr_print.h \
    src/organization_settings_form.h \
    src/libr_reports_gen_client.h \
    src/report_types.h

FORMS    += ui/mainwindow.ui \
    ui/book_tab.ui \
    ui/reader_tab.ui \
    ui/search_book_tab.ui \
    ui/search_reader_tab.ui \
    ui/reader_profile.ui \
    ui/book_profile.ui \
    ui/book_edit_tab.ui \
    ui/reader_edit_tab.ui \
    ui/give_book_form.ui \
    ui/return_book_form.ui \
    ui/remove_book_form.ui \
    ui/login_form.ui \
    ui/settings_form.ui \
    ui/network_settings_form.ui \
    ui/interface_settings_form.ui \
    ui/users_settings_form.ui \
    ui/get_book_id_form.ui \
    ui/get_reader_id_form.ui \
    ui/remove_reader_form.ui \
    ui/add_book_form.ui \
    ui/help_browser.ui \
    ui/about_form.ui \
    ui/statistics_form.ui \
    ui/table_form.ui \
    ui/organization_settings_form.ui

CONFIG += uitools

RESOURCES += \
    icons.qrc

